const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const STAGE_ENUM = [
  "pending_breaking",
  "breaking",
  "confirmation",
  "pending_transcribing",
  "completed",
];

const SlideSchema = new Schema({
  speakerNumber: { type: Number },
  silent: { type: Boolean, default: false },
  startTime: { type: Number },
  endTime: { type: Number },
});

const BrokenVideoSchema = new Schema({
  videoId: { type: String },
  title: { type: String },
  videoUrl: { type: String },
  langCode: { type: String },
  duration: { type: Number },
  numberOfSpeakers: { type: Number },
  currentSlideNumber: { type: Number },
  stage: { type: String, enum: STAGE_ENUM },
  slides: [SlideSchema],
  requestedBy: { type: String },
});

module.exports = mongoose.model("BrokenVideo", BrokenVideoSchema);
