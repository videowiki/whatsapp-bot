const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const STAGE_ENUM = ["lang_select", "transcribing", "completed"];

const SlideSchema = new Schema({
  text: { type: String },
  speakerNumber: { type: Number },
  silent: { type: Boolean, default: false },
  startTime: { type: Number },
  endTime: { type: Number },
  slidePosition: { type: Number },
  subslidePosition: { type: Number },
});

const TranscriptionSchema = new Schema({
  brokenVideo: { type: Schema.Types.ObjectId, ref: "BrokenVideo" },
  videoId: { type: String },
  videoUrl: { type: String },
  langCode: { type: String },
  currentSlideNumber: { type: Number },
  stage: { type: String, enum: STAGE_ENUM },
  slides: [SlideSchema],
});

module.exports = mongoose.model("Transcription", TranscriptionSchema);
