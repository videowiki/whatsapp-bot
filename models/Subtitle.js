const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const STAGE_ENUM = ["lang_select", "end_time", "completed"];

const SentenceSchema = new Schema({
  text: { type: String },
  endTime: { type: Number },
});

const SlideSchema = new Schema({
  text: { type: String },
  slidePosition: { type: Number },
  subslidePosition: { type: Number },
  startTime: { type: Number },
  endTime: { type: Number },
  sentences: [SentenceSchema],
  currentSentenceNumber: { type: Number, default: 0 },
  silent: { type: Boolean, default: false },
});

const SubtitlesSchema = new Schema({
  videoUrl: { type: String },
  articleId: { type: String },
  stage: { type: String, enum: STAGE_ENUM },
  slides: [SlideSchema],
  currentSlideNumber: { type: Number, default: 0 },
  langCode: { type: String },
});

module.exports = mongoose.model("Subtitles", SubtitlesSchema);
