const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const STAGE_ENUM = [
  "translation_type",
  "lang_from",
  "lang_to",
  "text_translation",
  "audio_translation",
  "completed",
];
const EDITING_STAGE_ENUM = ["pending", "editing_request", "editing_slide"];
const TRANSLATION_TYPE_ENUM = ["text", "audio", "text_audio"];

const SlideSchema = new Schema({
  number: { type: Number },
  text: { type: String },
  videoUrl: { type: String },
  audio: { type: String },
  slidePosition: { type: Number },
  subslidePosition: { type: Number },
  duration: { type: Number },
});

const TranslationSchema = new Schema({
  videoId: { type: String },
  articleId: { type: String },
  stage: { type: String, enum: STAGE_ENUM },
  originalSlides: [SlideSchema],
  translatedSlides: [SlideSchema],
  currentSlideNumber: { type: Number },
  translationType: { type: String, enum: TRANSLATION_TYPE_ENUM },
  textTranslated: { type: Boolean, default: false },
  audioTranslated: { type: Boolean, default: false },
  langFrom: { type: String },
  langTo: { type: String },
  editing: { type: Boolean, default: false },
  editingStage: { type: String, enum: EDITING_STAGE_ENUM },
  editingSlideNumber: { type: Number },
  action: { type: Boolean, default: false },
});

module.exports = mongoose.model("Translation", TranslationSchema);
