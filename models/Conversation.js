const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const STAGE_ENUM = ["option_select", "started", "completed"];
const TYPE_ENUM = [
  "translation",
  "brokenVideo",
  "transcription",
  "verification",
  "subtitle",
];

const ConversationSchema = new Schema({
  contactNumber: { type: String },
  stage: { type: String, enum: STAGE_ENUM },
  type: { type: String, enum: TYPE_ENUM },
  translation: { type: Schema.Types.ObjectId, ref: "Translation" },
  brokenVideo: { type: Schema.Types.ObjectId, ref: "BrokenVideo" },
  transcription: { type: Schema.Types.ObjectId, ref: "Transcription" },
  verification: { type: Schema.Types.ObjectId, ref: "Verification" },
  subtitle: { type: Schema.Types.ObjectId, ref: "Subtitle" },
});

module.exports = mongoose.model("Conversation", ConversationSchema);
