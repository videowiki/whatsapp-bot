const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const STAGE_ENUM = [
  "process_text",
  "correct_text",
  "comment_text",
  "process_voice",
  "comment_voice",
  "completed",
];
const TYPE_ENUM = ["text", "voice"];

const SlideSchema = new Schema({
  originalText: { type: String },
  translatedText: { type: String },
  correctedText: { type: String },
  comment: { type: String },
  valid: { type: Boolean, default: false },
  videoUrl: { type: String },
  translatedAudioUrl: { type: String },
  slidePosition: { type: Number },
  subslidePosition: { type: Number },
});

const VerificationSchema = new Schema({
  videoId: { type: String },
  originalArticleId: { type: String },
  translatedArticleId: { type: String },
  translatedArticleLangCode: { type: String },
  slides: [SlideSchema],
  textVerified: { type: Boolean, default: false },
  voiceVerified: { type: Boolean, default: false },
  currentSlideNumber: { type: Number },
  stage: { type: String, enum: STAGE_ENUM },
  type: { type: String, enum: TYPE_ENUM },
});

module.exports = mongoose.model("Verification", VerificationSchema);
