const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TYPE_ENUM = ["proofreading_ready"];

const UserActionSubscriptionSchema = new Schema({
  type: { type: String, enum: TYPE_ENUM },
  resourceId: { type: String },
  contactNumbers: [{ type: String }],
});

module.exports = mongoose.model(
  "UserActionSubscriptions",
  UserActionSubscriptionSchema
);
