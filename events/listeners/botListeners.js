const {
  sendText,
  sendMediaViaPath,
  sendMediaViaUrl,
} = require("../../turn.io");
const fs = require("fs");
const async = require("async");
const createChannel = require("../createChannel");
const QUEUES = require("../queues").QUEUES;

let channel;
createChannel()
  .then((ch) => {
    channel = ch;
    channel.prefetch(1);

    channel.assertQueue(QUEUES.WHATSPPP_SEND_MESSAGE_TO_USER_QUEUE, {
      durable: true,
    });
    channel.consume(
      QUEUES.WHATSPPP_SEND_MESSAGE_TO_USER_QUEUE,
      onSendMessageToUser,
      {
        noAck: false,
      }
    );
  })
  .catch((err) => {
    throw err;
  });

const onSendMessageToUser = (msg) => {
  // channel.ack(msg);
  const response = JSON.parse(msg.content.toString());
  console.log(response);
  const execFunc = [];
  // channel.ack(msg);
  if (response.videoUrl || response.videoPath) {
    if (response.textMessages.length) {
      response.textMessages.forEach((textMsg, i) => {
        if (response.videoIndex === i) {
          if (response.videoUrl) {
            execFunc.push((cb) => {
              sendMediaViaUrl(response.videoUrl, response.to, "video")
                .then(() => {
                  setTimeout(cb, 2000);
                })
                .catch((error) => {
                  cb();
                  console.log(error);
                });
            });
          }
          if (response.videoPath) {
            execFunc.push((cb) => {
              sendMediaViaPath(response.videoPath, response.to, "video")
                .then(() => {
                  setTimeout(cb, 2000);
                  fs.unlink(response.videoPath, (e) => {
                    if (e) console.log("removed file", e);
                  });
                })
                .catch((error) => {
                  cb();
                  console.log(error);
                });
            });
          }
          execFunc.push((cb) => {
            sendText(textMsg, response.to)
              .then(() => cb())
              .catch((error) => {
                cb();
                console.log(error);
              });
          });
        } else {
          execFunc.push((cb) => {
            sendText(textMsg, response.to)
              .then(() => cb())
              .catch((error) => {
                cb();
                console.log(error);
              });
          });
          if (
            response.videoIndex === response.textMessages.length &&
            i === response.textMessages.length - 1
          ) {
            if (response.videoUrl) {
              execFunc.push((cb) => {
                sendMediaViaUrl(response.videoUrl, response.to, "video")
                  .then(() => {
                    setTimeout(cb, 2000);
                  })
                  .catch((error) => {
                    cb();
                    console.log(error);
                  });
              });
            }
            if (response.videoPath) {
              execFunc.push((cb) => {
                sendMediaViaPath(response.videoPath, response.to, "video")
                  .then(() => {
                    setTimeout(cb, 2000);
                    fs.unlink(response.videoPath, (e) => {
                      if (e) console.log("removed file", e);
                    });
                  })
                  .catch((error) => {
                    cb();
                    console.log(error);
                  });
              });
            }
          }
        }
      });
    } else {
      if (response.videoUrl) {
        execFunc.push((cb) => {
          sendMediaViaUrl(response.videoUrl, response.to, "video")
            .then(() => cb())
            .catch((error) => {
              cb();
              console.log(error);
            });
        });
      }
      if (response.videoPath) {
        execFunc.push((cb) => {
          sendMediaViaPath(response.videoPath, response.to, "video")
            .then(() => {
              cb();
              fs.unlink(response.videoPath, (e) => {
                if (e) console.log("removed file", e);
              });
            })
            .catch((error) => {
              cb();
              console.log(error);
            });
        });
      }
    }
  } else if (response.audioPath) {
    if (response.textMessages.length) {
      response.textMessages.forEach((textMsg, i) => {
        if (response.audioIndex === i) {
          execFunc.push((cb) => {
            sendMediaViaPath(response.audioPath, response.to, "audio")
              .then(() => {
                setTimeout(cb, 2000);
                fs.unlink(response.audioPath, (e) => {
                  if (e) console.log("removed file", e);
                });
              })
              .catch((error) => {
                cb();
                console.log(error);
              });
          });
          execFunc.push((cb) => {
            sendText(textMsg, response.to)
              .then(() => cb())
              .catch((error) => {
                cb();
                console.log(error);
              });
          });
        } else {
          execFunc.push((cb) => {
            sendText(textMsg, response.to)
              .then(() => cb())
              .catch((error) => {
                cb();
                console.log(error);
              });
          });
        }
      });
    } else {
      execFunc.push((cb) => {
        sendMediaViaPath(response.audioPath, response.to, "audio")
          .then(() => {
            cb();
            fs.unlink(response.audioPath, (e) => {
              if (e) console.log("removed file", e);
            });
          })
          .catch((error) => {
            cb();
            console.log(error);
          });
      });
    }
  } else {
    response.textMessages.forEach((textMsg) => {
      execFunc.push((cb) => {
        sendText(textMsg, response.to)
          .then(() => cb())
          .catch((error) => {
            cb();
            console.log(error);
          });
      });
    });
  }

  async.series(execFunc, () => {
    channel.ack(msg);
  });
};

module.exports = { onSendMessageToUser };
