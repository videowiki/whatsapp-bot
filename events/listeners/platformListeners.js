const videoService = require("@videowiki/services/video")(
  process.env.VIDEO_SERVICE_API_ROOT
);
const BrokenVideo = require("../../models/BrokenVideo");
const UserActionSubscription = require("../../models/UserActionSubscription");
const { WHITELISTED_NUMBERS } = require("../../handlers/utils/constants");
const { videoPendingBreaking } = require("../emitters/platformEvents");
const { sendToUser } = require("../emitters/botEvents");
const createChannel = require("../createChannel");
const QUEUES = require("../queues").QUEUES;

let channel;
createChannel()
  .then((ch) => {
    channel = ch;
    channel.prefetch(1);

    channel.assertQueue(QUEUES.WHATSAPP_VIDEO_AVAILABLE_TO_CUT_QUEUE, {
      durable: true,
    });
    channel.assertQueue(QUEUES.WHATSAPP_NOTIFY_USER_VIDEO_PROOFREADING_READY, {
      durable: true,
    });

    channel.consume(
      QUEUES.WHATSAPP_VIDEO_AVAILABLE_TO_CUT_QUEUE,
      onVideoAvailableToBreak,
      { noAck: false }
    );
    channel.consume(
      QUEUES.WHATSAPP_NOTIFY_USER_VIDEO_PROOFREADING_READY,
      onVideoAvailableToProofread,
      { noAck: false }
    );
  })
  .catch((err) => {
    throw err;
  });
const onVideoAvailableToBreak = (msg) => {
  console.log(msg);

  const video = JSON.parse(msg.content.toString());
  console.log(video);

  createBrokenVideo(video)
    .then((brokenVideo) => {
      console.log("onVideoAvailableToCut created broken video", brokenVideo);
      channel.ack(msg);
    })
    .catch((err) => {
      channel.ack(msg);
      console.log(" onVideoAvailableToCut err ", err);
    });
};

const createBrokenVideo = (video, requestedBy) => {
  return new Promise((resolve, reject) => {
    BrokenVideo.create({
      videoId: video._id,
      title: video.title,
      videoUrl: video.url,
      duration: Math.floor(video.duration),
      numberOfSpeakers: video.numberOfSpeakers,
      stage: "pending_breaking",
      langCode: video.langCode,
      requestedBy: requestedBy || undefined,
    })
      .then((b) => {
        resolve(b);
        WHITELISTED_NUMBERS.forEach((number) => {
          sendToUser({
            to: number,
            textMessages: ["New video has been uploaded to be broken"],
          });
        });
        videoPendingBreaking({ videoId: video._id });
      })
      .catch(reject);
  });
};

const onVideoAvailableToProofread = (msg) => {
  const videoId = JSON.parse(msg.content.toString()).videoId;
  let video;
  channel.ack(msg);
  return videoService
    .findById(videoId)
    .then((v) => {
      video = v;
      return UserActionSubscription.findOne({
        type: "proofreading_ready",
        resourceId: videoId,
      });
    })
    .then((subscription) => {
      if (subscription && subscription.contactNumbers) {
        subscription.contactNumbers.forEach((contactNumber) => {
          sendToUser({
            to: contactNumber,
            textMessages: [
              `The video *"${video.title}"* is ready for proofreading`,
            ],
          });
        });
      }
    })
    .catch(console.err);
};
