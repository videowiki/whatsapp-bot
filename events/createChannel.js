const amqp = require("amqplib/callback_api");
const RABBITMQ_SERVER = process.env.RABBITMQ_SERVER;
let channel;

module.exports = () => {
  return new Promise((resolve, reject) => {
    if (channel) {
      return resolve(channel);
    }
    amqp.connect(RABBITMQ_SERVER, (error0, connection) => {
      if (error0) {
        reject(error0);
      }
      connection.createChannel((error1, ch) => {
        if (error1) {
          reject(error1);
        }
        channel = ch;
        resolve(ch);
      });
    });
  });
};
