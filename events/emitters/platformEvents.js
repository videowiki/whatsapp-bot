const { QUEUES } = require("../queues");
const createChannel = require("../createChannel");
let channel;
createChannel()
  .then((ch) => {
    channel = ch;
    channel.assertQueue(QUEUES.WHATSAPP_TRANSCRIBE_VIDEO_DONE_QUEUE, {
      durable: true,
    });
    channel.assertQueue(QUEUES.WHATSAPP_CUT_VIDEO_STARTED_QUEUE, {
      durable: true,
    });
    channel.assertQueue(QUEUES.WHATSAPP_CUT_VIDEO_DONE_QUEUE, {
      durable: true,
    });
    channel.assertQueue(QUEUES.WHATSAPP_TRANSLATION_STARTED_QUEUE, {
      durable: true,
    });
    channel.assertQueue(QUEUES.WHATSAPP_TRANSLATION_TEXT_CHANGED_QUEUE, {
      durable: true,
    });
    channel.assertQueue(QUEUES.WHATSAPP_TRANSLATION_AUDIO_CHANGED_QUEUE, {
      durable: true,
    });
  })
  .catch((err) => {
    throw err;
  });

const videoTranscribed = (data) => {
  channel.sendToQueue(
    QUEUES.WHATSAPP_TRANSCRIBE_VIDEO_DONE_QUEUE,
    Buffer.from(JSON.stringify(data)),
    {
      persistent: true,
    }
  );
};

const videoPendingBreaking = (data) => {
  channel.sendToQueue(
    QUEUES.WHATSAPP_CUT_VIDEO_STARTED_QUEUE,
    Buffer.from(JSON.stringify(data)),
    {
      persistent: true,
    }
  );
};

const videoBroken = (data) => {
  channel.sendToQueue(
    QUEUES.WHATSAPP_CUT_VIDEO_DONE_QUEUE,
    Buffer.from(JSON.stringify(data)),
    {
      persistent: true,
    }
  );
};

const translationStarted = (data) => {
  channel.sendToQueue(
    QUEUES.WHATSAPP_TRANSLATION_STARTED_QUEUE,
    Buffer.from(JSON.stringify(data)),
    {
      persistent: true,
    }
  );
};

const translationTextChanged = (data) => {
  channel.sendToQueue(
    QUEUES.WHATSAPP_TRANSLATION_TEXT_CHANGED_QUEUE,
    Buffer.from(JSON.stringify(data)),
    {
      persistent: true,
    }
  );
};


const translationAudioChanged = (data) => {
  channel.sendToQueue(
    QUEUES.WHATSAPP_TRANSLATION_AUDIO_CHANGED_QUEUE,
    Buffer.from(JSON.stringify(data)),
    {
      persistent: true,
    }
  );
};

module.exports = {
  videoPendingBreaking,
  videoBroken,
  videoTranscribed,
  translationStarted,
  translationTextChanged,
  translationAudioChanged,
};
