const { QUEUES } = require("../queues");
const createChannel = require("../createChannel");
let channel;
createChannel()
  .then((ch) => {
    channel = ch;
    channel.assertQueue(QUEUES.WHATSPPP_SEND_MESSAGE_TO_USER_QUEUE, {
      durable: true,
    });
  })
  .catch((err) => {
    throw err;
  });

const sendToUser = (data) => {
  channel.sendToQueue(
    QUEUES.WHATSPPP_SEND_MESSAGE_TO_USER_QUEUE,
    Buffer.from(JSON.stringify(data)),
    {
      persistent: true,
    }
  );
};

module.exports = { sendToUser };
