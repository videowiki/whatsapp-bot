const { exec } = require("child_process");
const request = require("superagent");
const fs = require("fs");
const mime = require("mime");

let token = "";
const loginEndpoint = "https://whatsapp.turn.io/v1/users/login";
const messageEndpoint = "https://whatsapp.turn.io/v1/messages";
const mediaEndpoint = "https://whatsapp.turn.io/v1/media";

const login = (username, password) => {
  const credentials = `${username}:${password}`;
  const authorizationToken = Buffer.from(credentials).toString("base64");
  return new Promise((resolve, reject) => {
    request
      .post(loginEndpoint)
      .send({ user: JSON.stringify(credentials) })
      .set("Authorization", `Basic ${authorizationToken}`)
      .then(({ text }) => {
        token = JSON.parse(text).users[0].token;
        if (token) resolve(token);
      })
      .catch(reject);
  });
};

const getMedia = (id) => {
  return new Promise((resolve, reject) => {
    request
      .get(`${mediaEndpoint}/${id}`)
      .set("Authorization", `Bearer ${token}`)
      .then((response) => resolve(response.body))
      .catch((error) => reject(error));
  });
};

const sendText = (body, to) => {
  return new Promise((resolve, reject) => {
    request
      .post(messageEndpoint)
      .send(
        JSON.stringify({
          preview_url: false,
          recipient_type: "individual",
          to,
          type: "text",
          text: {
            body,
          },
        })
      )
      .set("Authorization", `Bearer ${token}`)
      .set("Content-Type", "application/json")
      .then((response) => {
        resolve(JSON.parse(response.text).messages[0].id);
      })
      .catch(reject);
  });
};

const sendMediaViaUrl = (mediaUrl, to, type) => {
  return new Promise((resolve, reject) => {
    const mediaDownloadPath = `./${Date.now()}.${mediaUrl.split(".").pop()}`;
    const writeStream = fs.createWriteStream(mediaDownloadPath);
    request.get(mediaUrl).pipe(writeStream);
    writeStream.on("finish", () => {
      const readStream = fs.createReadStream(mediaDownloadPath);
      const buffers = [];
      readStream.on("data", (data) => {
        buffers.push(data);
      });
      readStream.on("end", () => {
        const actualContents = Buffer.concat(buffers);
        const mediaMimeType = mime.getType(mediaDownloadPath);
        fs.unlink(mediaDownloadPath, (error) => {
          if (error) console.log(error);
        });
        request
          .post(mediaEndpoint)
          .send(actualContents)
          .set("Authorization", `Bearer ${token}`)
          .set("Content-Type", mediaMimeType)
          .then((response) => {
            const id = JSON.parse(response.text).media[0].id;
            const type = mediaMimeType.split("/")[0];
            const responseMsg = {
              recipient_type: "individual",
              to,
              type,
              [type]: { id },
            };
            request
              .post(messageEndpoint)
              .send(JSON.stringify(responseMsg))
              .set("Authorization", `Bearer ${token}`)
              .set("Content-Type", "application/json")
              .then((response) => {
                resolve(JSON.parse(response.text).messages[0].id);
              })
              .catch((error) => {
                reject(error);
              });
          })
          .catch((error) => {
            reject(error);
          });
      });
    });
  });
};

const sendMediaViaPath = (mediaPath, to, type) => {
  return new Promise((resolve, reject) => {
    const mediaMimeType = mime.getType(mediaPath);
    const cmd = `curl -X POST ${mediaEndpoint} -H 'Authorization: Bearer ${token}' -H 'Content-Type: ${mediaMimeType}' --data-binary @${mediaPath}`;
    exec(cmd, (error, stdout, stderr) => {
      if (JSON.parse(stdout).media) {
        const id = JSON.parse(stdout).media[0].id;
        const responseMsg = {
          recipient_type: "individual",
          to,
          type,
          [type]: { id },
        };
        request
          .post(messageEndpoint)
          .send(JSON.stringify(responseMsg))
          .set("Authorization", `Bearer ${token}`)
          .set("Content-Type", "application/json")
          .then((response) => {
            resolve(JSON.parse(response.text).messages[0].id);
          })
          .catch((error) => {
            reject(error);
          });
      }
    });
  });
};

module.exports = {
  login,
  getMedia,
  sendText,
  sendMediaViaUrl,
  sendMediaViaPath,
};
