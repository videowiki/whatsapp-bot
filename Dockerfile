FROM hassanamin994/node_ffmpeg:6

WORKDIR /whatsapp-bot

ENV GOOGLE_APPLICATION_CREDENTIALS=/whatsapp-bot/gsc_creds.json

COPY . .
RUN npm install
EXPOSE 4000

CMD ["npm", "run", "docker:prod"]
