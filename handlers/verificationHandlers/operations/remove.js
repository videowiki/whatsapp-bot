const Conversation = require("../../../models/Conversation");
const Verification = require("../../../models/Verification");
const handleBreakingSelect = require("../../transcriptionHandlers/stages/breaking/select");
const handleTranscriptionSelect = require("../../transcriptionHandlers/stages/transcribing/select");
const handleSubtitlesSelect = require("../../subtitlesHandlers/stages/select");
const handleTranslationSelect = require("../../translationHandlers/stages/select");

const removeVerification = (
  msg,
  verification,
  conversation,
  newModel,
  translationType
) => {
  return new Promise((resolve, reject) => {
    Verification.deleteOne({ _id: verification._id })
      .then(() => {
        return Conversation.update(
          { _id: conversation._id },
          {
            $set: {
              stage: "option_select",
              type: null,
              verification: null,
            },
          }
        );
      })
      .then(() => {
        if (newModel === "BrokenVideo") {
          return handleBreakingSelect(msg, conversation)
            .then(resolve)
            .catch(reject);
        }
        if (newModel === "Transcription") {
          return handleTranscriptionSelect(msg, conversation)
            .then(resolve)
            .catch(reject);
        }
        if (newModel === "Subtitle") {
          return handleSubtitlesSelect(msg, conversation)
            .then(resolve)
            .catch(reject);
        }
        if (newModel === "Translation") {
          return handleTranslationSelect(msg, conversation, translationType)
            .then(resolve)
            .catch(reject);
        }
      })
      .catch(reject);
  });
};

module.exports = removeVerification;
