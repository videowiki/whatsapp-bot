const Conversation = require("../../../models/Conversation");
const Verification = require("../../../models/Verification");
const { fetchArticleById } = require("../../utils/api");
const {
  getVerificationSlides,
  convertAudioToMP3,
} = require("../../utils/helpers");

const handlePlatformVerificationAction = (msg, translatedArticleId, type) => {
  return new Promise((resolve, reject) => {
    let originalArticle;
    let translatedArticle;
    let verification;
    return fetchArticleById(translatedArticleId)
      .then((ta) => {
        translatedArticle = ta;
        if (
          translatedArticle &&
          translatedArticle.articleType === "translation" &&
          ((translatedArticle.translationStatus === "text_verification" &&
            type === "text") ||
            (translatedArticle.translationStatus === "voice_verification" &&
              type === "voice"))
        ) {
          return fetchArticleById(translatedArticle.originalArticle);
        }
        return resolve({
          textMessages: [
            "The article must be of type translation and in the verification stage",
          ],
          to: msg.sender,
        });
      })
      .then((oa) => {
        originalArticle = oa;
        const slides = getVerificationSlides(
          originalArticle,
          translatedArticle
        );
        return Verification.create({
          videoId: originalArticle.video,
          originalArticleId: originalArticle._id,
          translatedArticleId: translatedArticle._id,
          currentSlideNumber: 1,
          stage: type === "text" ? "process_text" : "process_voice",
          slides,
          type,
          translatedArticleLangCode: translatedArticle.langCode,
        });
      })
      .then((v) => {
        verification = v;
        return Conversation.create({
          contactNumber: msg.sender,
          type: "verification",
          verification: verification._id,
          stage: "started",
        });
      })
      .then(() => {
        if (verification.type === "text") {
          return resolve({
            videoUrl: verification.slides[0].videoUrl,
            videoIndex: 0,
            textMessages: [
              `*Original Text:*\n${verification.slides[0].originalText}\n\n*Text Translated by Translator:*\n${verification.slides[0].translatedText}`,
              `Type *Perfect* to accept the translation\n\nType *Correct* to correct the translation\n\nType *Comment* to add comment for the translator`,
            ],
            to: msg.sender,
          });
        }
        if (verification.type === "voice") {
          return convertAudioToMP3(verification.slides[0].translatedAudioUrl)
            .then((convertedPath) => {
              return resolve({
                audioPath: convertedPath,
                audioIndex: 0,
                textMessages: [
                  `*Original Text:*\n${verification.slides[0].originalText}\n\n*Text Translated by Translator:*\n${verification.slides[0].translatedText}`,
                  `Type *Perfect* to accept the translation\n\nType *Comment* to add comment for the translator`,
                ],
                to: msg.sender,
              });
            })
            .catch(reject);
        }
      })
      .catch(reject);
  });
};

module.exports = handlePlatformVerificationAction;
