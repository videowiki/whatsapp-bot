const handleTextVerificationStage = require("./text");
const handleVoiceVerificationStage = require("./voice");
const removeVerification = require("../operations/remove");
const { isAction } = require("../../utils/helpers");
const { ACTION_CODES } = require("../../utils/constants");

const handleVerificationStages = (msg, verification, conversation) => {
  return new Promise((resolve, reject) => {
    if (msg.type !== "text") {
      return resolve({
        textMessages: ["Please use text messages"],
        to: msg.sender,
      });
    }
    if (isAction(msg.content, ACTION_CODES.PROOFREAD_BREAK_VIDEOS)) {
      return removeVerification(msg, verification, conversation, "BrokenVideo")
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.PROOFREAD_TYPE_TEXT)) {
      return removeVerification(
        msg,
        verification,
        conversation,
        "Transcription"
      )
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.ADD_SUBTITLES)) {
      return removeVerification(msg, verification, conversation, "Subtitle")
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.SELECT_TEXT_TRANSLATION)) {
      return removeVerification(
        msg,
        verification,
        conversation,
        "Translation",
        "text"
      )
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.SELECT_AUDIO_TRANSLATION)) {
      return removeVerification(
        msg,
        verification,
        conversation,
        "Translation",
        "audio"
      )
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.SELECT_TEXT_AND_AUDIO_TRANSLATION)) {
      return removeVerification(
        msg,
        verification,
        conversation,
        "Translation",
        "text_audio"
      )
        .then(resolve)
        .catch(reject);
    }
    if (verification.type === "text") {
      return handleTextVerificationStage(msg, verification, conversation)
        .then(resolve)
        .catch(reject);
    }
    if (verification.type === "voice") {
      return handleVoiceVerificationStage(msg, verification, conversation)
        .then(resolve)
        .catch(reject);
    }
  });
};

module.exports = handleVerificationStages;
