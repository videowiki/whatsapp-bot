const Verification = require("../../../models/Verification");
const Conversation = require("../../../models/Conversation");

const handleTextVerificationStage = (msg, verification, conversation) => {
  return new Promise((resolve, reject) => {
    if (verification.stage === "process_text") {
      return handleProcessTextStage(msg, verification, conversation)
        .then(resolve)
        .catch(reject);
    }
    if (verification.stage === "correct_text") {
      return handleCorrectTextStage(msg, verification, conversation)
        .then(resolve)
        .catch(reject);
    }
    if (verification.stage === "comment_text") {
      return handleCommentTextStage(msg, verification, conversation)
        .then(resolve)
        .catch(reject);
    }
  });
};

const handleProcessTextStage = (msg, verification, conversation) => {
  return new Promise((resolve, reject) => {
    if (msg.content.toLowerCase() === "perfect") {
      return handlePerfectTextSelect(msg, verification, conversation)
        .then(resolve)
        .catch(reject);
    }
    if (msg.content.toLowerCase() === "correct") {
      return handleCorrectTextSelect(msg, verification)
        .then(resolve)
        .catch(reject);
    }
    if (msg.content.toLowerCase() === "comment") {
      return handleCommentTextSelect(msg, verification)
        .then(resolve)
        .catch(reject);
    }
    return resolve({
      textMessages: ["Invalid input"],
      to: msg.sender,
    });
  });
};

const handlePerfectTextSelect = (msg, verification, conversation) => {
  return new Promise((resolve, reject) => {
    const check =
      verification.currentSlideNumber === verification.slides.length;
    verification.slides[verification.currentSlideNumber - 1].valid = true;
    const updates = {
      currentSlideNumber: check ? 0 : verification.currentSlideNumber + 1,
      stage: check ? "completed" : "process_text",
      textVerified: check,
      slides: verification.slides,
    };
    return Verification.findOneAndUpdate(
      { _id: verification._id },
      { $set: updates },
      { new: true }
    )
      .then((verification) => {
        if (verification.stage === "completed") {
          return Conversation.updateOne(
            { _id: conversation._id },
            {
              $set: {
                stage: "completed",
              },
            }
          )
            .then(() => {
              return resolve({
                textMessages: ["The text verification is completed"],
                to: msg.sender,
              });
            })
            .catch(reject);
        }
        return resolve({
          videoUrl:
            verification.slides[verification.currentSlideNumber - 1].videoUrl,
          videoIndex: 0,
          textMessages: [
            `*Original Text:* ${
              verification.slides[verification.currentSlideNumber - 1]
                .originalText
            }\n\n*Text Translated by Translator:* ${
              verification.slides[verification.currentSlideNumber - 1]
                .translatedText
            }`,
            `Type *Perfect* to accept the translation\n\nType *Correct* to correct the translation\n\nType *Comment* to add comment for the translator`,
          ],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

const handleCorrectTextSelect = (msg, verification) => {
  return new Promise((resolve, reject) => {
    Verification.updateOne(
      { _id: verification._id },
      { $set: { stage: "correct_text" } }
    )
      .then(() => {
        return resolve({
          textMessages: ["Please send the corrected translation"],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

const handleCommentTextSelect = (msg, verification) => {
  return new Promise((resolve, reject) => {
    Verification.updateOne(
      { _id: verification._id },
      { $set: { stage: "comment_text" } }
    )
      .then(() => {
        return resolve({
          textMessages: ["Please write your comment for the translator"],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

const handleCorrectTextStage = (msg, verification, conversation) => {
  return new Promise((resolve, reject) => {
    const check =
      verification.currentSlideNumber === verification.slides.length;
    verification.slides[verification.currentSlideNumber - 1].correctedText =
      msg.content;
    const updates = {
      currentSlideNumber: check ? 0 : verification.currentSlideNumber + 1,
      stage: check ? "completed" : "process_text",
      textVerified: check,
      //[`slides.${verification.currentSlideNumber-1}.correctedText`]: msg.content,
      slides: verification.slides,
    };
    return Verification.findOneAndUpdate(
      { _id: verification._id },
      { $set: updates },
      { new: true }
    )
      .then((verification) => {
        // EMIT SLIDE TEXT CHANGE EVENT
        if (verification.stage === "completed") {
          return Conversation.updateOne(
            { _id: conversation._id },
            {
              $set: {
                stage: "completed",
              },
            }
          )
            .then(() => {
              return resolve({
                textMessages: ["The text verification is completed"],
                to: msg.sender,
              });
            })
            .catch(reject);
        }
        return resolve({
          videoUrl:
            verification.slides[verification.currentSlideNumber - 1].videoUrl,
          videoIndex: 0,
          textMessages: [
            `*Original Text:* ${
              verification.slides[verification.currentSlideNumber - 1]
                .originalText
            }\n\n*Text Translated by Translator:* ${
              verification.slides[verification.currentSlideNumber - 1]
                .translatedText
            }`,
            `Type *Perfect* to accept the translation\n\nType *Correct* to correct the translation\n\nType *Comment* to add comment for the translator`,
          ],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

const handleCommentTextStage = (msg, verification, conversation) => {
  return new Promise((resolve, reject) => {
    const check =
      verification.currentSlideNumber === verification.slides.length;
    verification.slides[verification.currentSlideNumber - 1].comment =
      msg.content;
    const updates = {
      currentSlideNumber: check ? 0 : verification.currentSlideNumber + 1,
      stage: check ? "completed" : "process_text",
      textVerified: check,
      slides: verification.slides,
    };
    return Verification.findOneAndUpdate(
      { _id: verification._id },
      { $set: updates },
      { new: true }
    )
      .then((verification) => {
        if (verification.stage === "completed") {
          return Conversation.updateOne(
            { _id: conversation._id },
            {
              $set: {
                stage: "completed",
              },
            }
          )
            .then(() => {
              return resolve({
                textMessages: ["The text verification is completed"],
                to: msg.sender,
              });
            })
            .catch(reject);
        }
        return resolve({
          videoUrl:
            verification.slides[verification.currentSlideNumber - 1].videoUrl,
          videoIndex: 0,
          textMessages: [
            `*Original Text:* ${
              verification.slides[verification.currentSlideNumber - 1]
                .originalText
            }\n\n*Text Translated by Translator:* ${
              verification.slides[verification.currentSlideNumber - 1]
                .translatedText
            }`,
            `Type *Perfect* to accept the translation\n\nType *Correct* to correct the translation\n\nType *Comment* to add comment for the translator`,
          ],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

module.exports = handleTextVerificationStage;
