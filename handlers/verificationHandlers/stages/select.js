const { conversationMessages } = require("../../utils/constants");
const handleVerificationSelect = (msg) => {
  return Promise.resolve({
    textMessages: [
      "The translation approving is allowed for the platform verifiers only",
      conversationMessages.mainMenu,
    ],
    to: msg.sender,
  });
};

module.exports = handleVerificationSelect;
