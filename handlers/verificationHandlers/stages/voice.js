const Verification = require("../../../models/Verification");
const Conversation = require("../../../models/Conversation");
const { convertAudioToMP3 } = require("../../utils/helpers");

const handleVoiceVerificationStage = (msg, verification, conversation) => {
  return new Promise((resolve, reject) => {
    if (verification.stage === "process_voice") {
      return handleProcessVoiceStage(msg, verification, conversation)
        .then(resolve)
        .catch(reject);
    }
    if (verification.stage === "comment_voice") {
      return handleCommentVoiceStage(msg, verification, conversation)
        .then(resolve)
        .catch(reject);
    }
  });
};

const handleProcessVoiceStage = (msg, verification, conversation) => {
  return new Promise((resolve, reject) => {
    if (msg.content.toLowerCase() === "perfect") {
      return handlePerfectVoiceSelect(msg, verification, conversation)
        .then(resolve)
        .catch(reject);
    }
    if (msg.content.toLowerCase() === "comment") {
      return handleCommentVoiceSelect(msg, verification)
        .then(resolve)
        .catch(reject);
    }
    return resolve({
      textMessages: ["Invalid input"],
      to: msg.sender,
    });
  });
};

const handlePerfectVoiceSelect = (msg, verification, conversation) => {
  return new Promise((resolve, reject) => {
    const check =
      verification.currentSlideNumber === verification.slides.length;
    verification.slides[verification.currentSlideNumber - 1].valid = true;
    const updates = {
      currentSlideNumber: check ? 0 : verification.currentSlideNumber + 1,
      stage: check ? "completed" : "process_voice",
      voiceVerified: check,
      slides: verification.slides,
    };
    return Verification.findOneAndUpdate(
      { _id: verification._id },
      { $set: updates },
      { new: true }
    )
      .then((verification) => {
        if (verification.stage === "completed") {
          return Conversation.updateOne(
            { _id: conversation._id },
            {
              $set: {
                stage: "completed",
              },
            }
          )
            .then(() => {
              return resolve({
                textMessages: ["The voice verification is completed"],
                to: msg.sender,
              });
            })
            .catch(reject);
        }
        return convertAudioToMP3(
          verification.slides[verification.currentSlideNumber - 1]
            .translatedAudioUrl
        )
          .then((convertedPath) => {
            return resolve({
              audioPath: convertedPath,
              audioIndex: 0,
              textMessages: [
                `*Original Text:*\n${
                  verification.slides[verification.currentSlideNumber - 1]
                    .originalText
                }\n\n*Text Translated by Translator:*\n${
                  verification.slides[verification.currentSlideNumber - 1]
                    .translatedText
                }`,
                `Type *Perfect* to accept the translation\n\nType *Comment* to add comment for the translator`,
              ],
              to: msg.sender,
            });
          })
          .catch(reject);
      })
      .catch(reject);
  });
};

const handleCommentVoiceSelect = (msg, verification) => {
  return new Promise((resolve, reject) => {
    Verification.updateOne(
      { _id: verification._id },
      { $set: { stage: "comment_voice" } }
    )
      .then(() => {
        return resolve({
          textMessages: ["Please write your comment for the translator"],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

const handleCommentVoiceStage = (msg, verification, conversation) => {
  return new Promise((resolve, reject) => {
    const check =
      verification.currentSlideNumber === verification.slides.length;
    verification.slides[verification.currentSlideNumber - 1].comment =
      msg.content;
    const updates = {
      currentSlideNumber: check ? 0 : verification.currentSlideNumber + 1,
      stage: check ? "completed" : "process_voice",
      voiceVerified: check,
      slides: verification.slides,
    };
    return Verification.findOneAndUpdate(
      { _id: verification._id },
      { $set: updates },
      { new: true }
    )
      .then((verification) => {
        if (verification.stage === "completed") {
          return Conversation.updateOne(
            { _id: conversation._id },
            {
              $set: {
                stage: "completed",
              },
            }
          )
            .then(() => {
              return resolve({
                textMessages: ["The voice verification is completed"],
                to: msg.sender,
              });
            })
            .catch(reject);
        }
        return convertAudioToMP3(
          verification.slides[verification.currentSlideNumber - 1]
            .translatedAudioUrl
        )
          .then((convertedPath) => {
            return resolve({
              audioPath: convertedPath,
              audioIndex: 0,
              textMessages: [
                `*Original Text:*\n${
                  verification.slides[verification.currentSlideNumber - 1]
                    .originalText
                }\n\n*Text Translated by Translator:*\n${
                  verification.slides[verification.currentSlideNumber - 1]
                    .translatedText
                }`,
                `Type *Perfect* to accept the translation\n\nType *Comment* to add comment for the translator`,
              ],
              to: msg.sender,
            });
          })
          .catch(reject);
      })
      .catch(reject);
  });
};

module.exports = handleVoiceVerificationStage;
