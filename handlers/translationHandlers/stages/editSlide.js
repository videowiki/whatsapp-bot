const fs = require("fs");
const path = require("path");
const { getMedia } = require("../../../turn.io");
const uuid = require("uuid").v4;
const musicMetadata = require("music-metadata");
const storageService = require("@videowiki/services/storage")(
  process.env.STORAGE_SERVICE_API_ROOT
);
const stringSimilarity = require("string-similarity");
const Translation = require("../../../models/Translation");
const {
  getFileDuration,
  getFileDurationFromBuffer,
  detectLang,
  translateText,
} = require("../../utils/helpers");
const { isoLangs, translationMessages } = require("../../utils/constants");
const {
  translationTextChanged,
  translationAudioChanged,
} = require("../../../events/emitters/platformEvents");

const handleEditingSlide = (msg, translation, conversation) => {
  return new Promise((resolve, reject) => {
    if (msg.type !== "text" && translation.stage === "text_translation") {
      return resolve({
        textMessages: [translationMessages.invalidText],
        to: msg.sender,
      });
    }
    if (msg.type !== "voice" && translation.stage === "audio_translation") {
      return resolve({
        textMessages: [translationMessages.invalidVoice],
        to: msg.sender,
      });
    }
    if (translation.stage === "audio_translation") {
      let originalDuration;
      return getFileDuration(
        translation.originalSlides[translation.editingSlideNumber - 1].audio
      )
        .then((od) => {
          originalDuration = od;
          return getMedia(msg.content);
        })
        .then((buffer) => {
          return getFileDurationFromBuffer(buffer);
        })
        .then((translationDuration) => {
          if (
            Number(translationDuration.toFixed(1)) >
            Number(originalDuration.toFixed(1))
          ) {
            return resolve({
              textMessages: [translationMessages.longVoice],
              to: msg.sender,
            });
          }
          let updatedTranslation;
          let buffer;
          let filePath;
          let extension;
          let editingSlideNumberRef = translation.editingSlideNumber;
          translation.translatedSlides[
            translation.editingSlideNumber - 1
          ].audio = msg.content;
          const updates = {
            editing: false,
            editingStage: "pending",
            editingSlideNumber: 0,
            translatedSlides: translation.translatedSlides,
          };
          return Translation.findOneAndUpdate(
            { _id: translation._id },
            { $set: updates },
            { new: true }
          )
            .then((ut) => {
              updatedTranslation = ut;
              return getMedia(msg.content);
            })
            .then((bf) => {
              buffer = bf;
              return musicMetadata.parseBuffer(buffer);
            })
            .then((metadata) => {
              return new Promise((resolve, reject) => {
                extension = metadata.format.container.toLowerCase();
                filePath = path.join(__dirname, `${uuid()}.${extension}`);
                fs.writeFile(filePath, buffer, (err) => {
                  if (err) reject(err);
                  console.log("The file has been saved!");
                  return resolve(filePath);
                });
              });
            })
            .then((filePath) => {
              return storageService.saveFile(
                "whatsapp-bot/voices",
                filePath.split("/").pop(),
                fs.createReadStream(filePath)
              );
            })
            .then(({ url }) => {
              fs.unlink(filePath, (err) => {
                if (err) console.log("removed file", err);
              });
              translationAudioChanged({
                videoId: updatedTranslation.videoId,
                langTo: updatedTranslation.langTo,
                audioUrl: url,
                contactNumber: conversation.contactNumber,
                slidePosition:
                  updatedTranslation.translatedSlides[editingSlideNumberRef - 1]
                    .slidePosition,
                subslidePosition:
                  updatedTranslation.translatedSlides[editingSlideNumberRef - 1]
                    .subslidePosition,
                // completed: false,
                action: updatedTranslation.action,
              });
              if (updatedTranslation.currentSlideNumber === 0) {
                return resolve({
                  textMessages: [
                    `${translationMessages.menuTip}\n\n${translationMessages.mistakeTip}\n\n${translationMessages.changeVideoTip}\n\n${translationMessages.confirmAudio}`,
                  ],
                  to: msg.sender,
                });
              }
              const resolved = {
                videoUrl:
                  updatedTranslation.originalSlides[
                    updatedTranslation.currentSlideNumber - 1
                  ].videoUrl,
                videoIndex: 2,
                textMessages: [
                  `${translationMessages.voiceTip} ${Math.floor(
                    updatedTranslation.originalSlides[
                      updatedTranslation.currentSlideNumber - 1
                    ].duration
                  )}`,
                  `${translationMessages.menuTip}\n\n${translationMessages.mistakeTip}\n\n${translationMessages.changeVideoTip}`,
                  `Slide ${updatedTranslation.currentSlideNumber}/${
                    updatedTranslation.originalSlides.length
                  }\n\n${isoLangs[updatedTranslation.langFrom].name}:\n\n${
                    updatedTranslation.originalSlides[
                      updatedTranslation.currentSlideNumber - 1
                    ].text
                  }`,
                  `Maximum time: ${Math.floor(
                    updatedTranslation.originalSlides[
                      updatedTranslation.currentSlideNumber - 1
                    ].duration
                  )} seconds`,
                ],
                to: msg.sender,
              };
              if (updatedTranslation.textTranslated) {
                resolved.textMessages[1] += `\n\n${
                  isoLangs[updatedTranslation.langTo].name
                }:\n\n${
                  updatedTranslation.translatedSlides[
                    updatedTranslation.currentSlideNumber - 1
                  ].text
                }`;
              }
              return resolve(resolved);
            })
            .catch(reject);
        })
        .catch(reject);
    }
    if (translation.stage === "text_translation") {
      return detectLang(msg.content)
        .then((langCode) => {
          if (translation.langTo === langCode) {
            return translateText(
              translation.originalSlides[translation.editingSlideNumber - 1]
                .text,
              translation.langTo
            )
              .then((translatedText) => {
                const similarity = stringSimilarity.compareTwoStrings(
                  msg.content,
                  translatedText
                );
                if (similarity < 0.1) {
                  return resolve({
                    textMessages: [translationMessages.spamError],
                    to: msg.sender,
                  });
                }
                if (similarity < 0.2) {
                  return resolve({
                    textMessages: [translationMessages.spamWarning],
                    to: msg.sender,
                  });
                }
                if (similarity >= 0.2) {
                  let editingSlideNumberRef = translation.editingSlideNumber;
                  translation.translatedSlides[
                    translation.editingSlideNumber - 1
                  ].text = msg.content;
                  const updates = {
                    editing: false,
                    editingStage: "pending",
                    editingSlideNumber: 0,
                    translatedSlides: translation.translatedSlides,
                  };

                  return Translation.findOneAndUpdate(
                    { _id: translation._id },
                    { $set: updates },
                    { new: true }
                  )
                    .then((updatedTranslation) => {
                      console.log({
                        videoId: updatedTranslation.videoId,
                        langTo: updatedTranslation.langTo,
                        text:
                          updatedTranslation.translatedSlides[
                            editingSlideNumberRef - 1
                          ].text,
                        contactNumber: conversation.contactNumber,
                        slidePosition:
                          updatedTranslation.translatedSlides[
                            editingSlideNumberRef - 1
                          ].slidePosition,
                        subslidePosition:
                          updatedTranslation.translatedSlides[
                            editingSlideNumberRef - 1
                          ].subslidePosition,
                        // completed: updatedTranslation.textTranslated,
                        action: updatedTranslation.action,
                      });
                      translationTextChanged({
                        videoId: updatedTranslation.videoId,
                        langTo: updatedTranslation.langTo,
                        text:
                          updatedTranslation.translatedSlides[
                            editingSlideNumberRef - 1
                          ].text,
                        contactNumber: conversation.contactNumber,
                        slidePosition:
                          updatedTranslation.translatedSlides[
                            editingSlideNumberRef - 1
                          ].slidePosition,
                        subslidePosition:
                          updatedTranslation.translatedSlides[
                            editingSlideNumberRef - 1
                          ].subslidePosition,
                        completed: updatedTranslation.textTranslated,
                        action: updatedTranslation.action,
                      });
                      if (updatedTranslation.currentSlideNumber === 0) {
                        return resolve({
                          textMessages: [
                            `${translationMessages.menuTip}\n\n${translationMessages.mistakeTip}\n\n${translationMessages.changeVideoTip}\n\n${translationMessages.confirmText}`,
                          ],
                          to: msg.sender,
                        });
                      }
                      return resolve({
                        videoUrl:
                          updatedTranslation.originalSlides[
                            updatedTranslation.currentSlideNumber - 1
                          ].videoUrl,
                        videoIndex: 1,
                        textMessages: [
                          `${translationMessages.menuTip}\n\n${translationMessages.mistakeTip}\n\n${translationMessages.changeVideoTip}`,
                          `Slide ${updatedTranslation.currentSlideNumber}/${
                            updatedTranslation.originalSlides.length
                          }\n\n${
                            isoLangs[updatedTranslation.langFrom].name
                          }:\n\n${
                            updatedTranslation.originalSlides[
                              updatedTranslation.currentSlideNumber - 1
                            ].text
                          }`,
                        ],
                        to: msg.sender,
                      });
                    })
                    .catch(reject);
                }
              })
              .catch(reject);
          }
          return resolve({
            textMessages: [
              translationMessages.langDetectionError.replace(
                "%l%",
                isoLangs[translation.langTo].name
              ),
            ],
            to: msg.sender,
          });
        })
        .catch(reject);
    }
  });
};

module.exports = handleEditingSlide;
