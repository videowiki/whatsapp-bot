const handleEditTranslation = require("./edit");
const handleEndOfTextTranslation = require("./endOfText");
const handleEndOfAudioTranslation = require("./endOfAudio");
const handleMistake = require("./mistake");
const handleChangeVideo = require("./changeVideo");
const removeTranslation = require("../operations/remove");
const resetTranslation = require("../operations/reset");
const handleTextTranslationStage = require("./text");
const handleAudioTranslationStage = require("./audio");

const { isAction } = require("../../utils/helpers");
const {
  ACTION_CODES,
  conversationMessages,
  translationMessages,
} = require("../../utils/constants");

const handleStartedTranslationOptions = (msg, translation, conversation) => {
  return new Promise((resolve, reject) => {
    if (translation.editing) {
      return handleEditTranslation(msg, translation, conversation)
        .then(resolve)
        .catch(reject);
    }
    if (
      isAction(msg.content, ACTION_CODES.CONFIRM) &&
      translation.currentSlideNumber === 0 &&
      translation.stage === "text_translation" &&
      translation.textTranslated
    ) {
      return handleEndOfTextTranslation(msg, translation, conversation)
        .then(resolve)
        .catch(reject);
    }
    if (
      isAction(msg.content, ACTION_CODES.CONFIRM) &&
      translation.currentSlideNumber === 0 &&
      translation.stage === "audio_translation" &&
      translation.audioTranslated
    ) {
      return handleEndOfAudioTranslation(msg, translation, conversation)
        .then(resolve)
        .catch(reject);
    }
    if (
      isAction(msg.content, ACTION_CODES.MISTAKE) &&
      translation.currentSlideNumber !== 1
    ) {
      return handleMistake(msg, translation).then(resolve).catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.CHANGE_VIDEO)) {
      return handleChangeVideo(msg, translation, conversation)
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.SHOW_MENU)) {
      return resolve({
        textMessages: [conversationMessages.mainMenu],
        to: msg.sender,
      });
    }
    if (isAction(msg.content, ACTION_CODES.PROOFREAD_BREAK_VIDEOS)) {
      return removeTranslation(msg, translation, conversation, "BrokenVideo")
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.PROOFREAD_TYPE_TEXT)) {
      return removeTranslation(msg, translation, conversation, "Transcription")
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.ADD_SUBTITLES)) {
      return removeTranslation(msg, translation, conversation, "Subtitle")
        .then(resolve)
        .catch(reject);
    }
    if (
      isAction(msg.content, ACTION_CODES.VERIFY_TEXT) ||
      isAction(msg.content, ACTION_CODES.VERIFY_TEXT)
    ) {
      return removeTranslation(msg, translation, conversation, "Verification")
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.SELECT_TEXT_TRANSLATION)) {
      return resetTranslation(msg, translation, "text")
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.SELECT_AUDIO_TRANSLATION)) {
      return resetTranslation(msg, translation, "audio")
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.SELECT_TEXT_AND_AUDIO_TRANSLATION)) {
      return resetTranslation(msg, translation, "text_audio")
        .then(resolve)
        .catch(reject);
    }
    if (
      translation.currentSlideNumber === 0 &&
      (translation.textTranslated || translation.audioTranslated)
    ) {
      return resolve({
        textMessages: [translationMessages.invalidInput],
        to: msg.sender,
      });
    }
    if (translation.stage === "text_translation") {
      return handleTextTranslationStage(msg, translation, conversation)
        .then(resolve)
        .catch(reject);
    }
    if (translation.stage === "audio_translation") {
      return handleAudioTranslationStage(msg, translation, conversation)
        .then(resolve)
        .catch(reject);
    }
  });
};

module.exports = handleStartedTranslationOptions;
