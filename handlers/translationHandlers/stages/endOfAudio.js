const Conversation = require("../../../models/Conversation");
const { translationMessages } = require("../../utils/constants");

const handleEndOfAudioTranslation = (msg, translation, conversation) => {
  return new Promise((resolve, reject) => {
    translation.stage = "completed";
    translation
      .save()
      .then(() => {
        return Conversation.update(
          { _id: conversation._id },
          {
            $set: {
              stage: "completed",
            },
          }
        );
      })
      .then(() => {
        return resolve({
          textMessages: [translationMessages.success],
          to: msg.sender,
        });
      })

      .catch(reject);
  });
};

module.exports = handleEndOfAudioTranslation;
