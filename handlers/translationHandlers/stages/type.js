const startTranslation = require("./start");
const { sendToUser } = require("../../../events/emitters/botEvents");

const handleTranslationTypeStage = (msg, translation) => {
  return new Promise((resolve, reject) => {
    if (msg.type !== "text") {
      return resolve({
        textMessages: ["Please use text messages"],
        to: msg.sender,
      });
    }
    if (msg.content === "1") {
      return startTranslation(msg, translation, "text")
        .then((response) => {
          sendToUser({
            videoUrl:
              "https://tailoredvideowiki.s3-eu-west-1.amazonaws.com/static/tutorials_media/whatsapp_tutorials/stage3_compressed.mp4",
            videoIndex: 1,
            textMessages: [
              "Here's a quick tutorial for how to translate text on WhatsApp",
            ],
            to: msg.sender,
          });
          resolve(response);
        })
        .catch(reject);
    }
    if (msg.content === "2") {
      return startTranslation(msg, translation, "audio")
        .then((response) => {
          sendToUser({
            videoUrl:
              "https://tailoredvideowiki.s3-eu-west-1.amazonaws.com/static/tutorials_media/whatsapp_tutorials/stage4_compressed.mp4",
            videoIndex: 1,
            textMessages: [
              "Here's a quick tutorial for how to add voice-over on WhatsApp",
            ],
            to: msg.sender,
          });
          resolve(response);
        })
        .catch(reject);
    }
    if (msg.content === "3") {
      return startTranslation(msg, translation, "text_audio")
        .then((response) => {
          sendToUser({
            videoUrl:
              "https://tailoredvideowiki.s3-eu-west-1.amazonaws.com/static/tutorials_media/whatsapp_tutorials/stage6_compressed.mp4",
            videoIndex: 1,
            textMessages: [
              "Here's a quick tutorial for how to translate text and add voice-over on WhatsApp",
            ],
            to: msg.sender,
          });
          resolve(response);
        })
        .catch(reject);
    }
    return resolve({
      textMessages: ["Invalid input"],
      to: msg.sender,
    });
  });
};

module.exports = handleTranslationTypeStage;
