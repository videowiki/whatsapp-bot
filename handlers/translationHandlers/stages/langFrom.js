const Translation = require("../../../models/Translation");
const { fetchTranslationArticle } = require("../../utils/api");
const {
  getTranslationSlides,
  getLangInEnglish,
  getLangCode,
} = require("../../utils/helpers");
const { translationMessages } = require("../../utils/constants");

const handleLangFromStage = (msg, translation) => {
  return new Promise((resolve, reject) => {
    const nativeLangCode = getLangCode(msg.content);
    if (nativeLangCode) {
      return fetchTranslationArticle(nativeLangCode)
        .then((article) => {
          if (!article) {
            return resolve({
              textMessages: [translationMessages.noArticle],
              to: msg.sender,
            });
          }
          const { originalSlides, translatedSlides } = getTranslationSlides(
            article
          );
          return Translation.update(
            { _id: translation._id },
            {
              $set: {
                articleId: article._id,
                videoId: article.video,
                langFrom: nativeLangCode,
                stage: "lang_to",
                originalSlides: originalSlides,
                translatedSlides: translatedSlides,
              },
            }
          )
            .then(() => {
              return resolve({
                textMessages: [translationMessages.langToQ],
                to: msg.sender,
              });
            })
            .catch(reject);
        })
        .catch(reject);
    }
    return getLangInEnglish(msg.content)
      .then((langName) => {
        const code = getLangCode(langName);
        if (code) {
          return fetchTranslationArticle(code)
            .then((article) => {
              if (!article) {
                return resolve({
                  textMessages: [translationMessages.noArticle],
                  to: msg.sender,
                });
              }
              const { originalSlides, translatedSlides } = getTranslationSlides(
                article
              );
              return Translation.update(
                { _id: translation._id },
                {
                  $set: {
                    articleId: article._id,
                    langFrom: code,
                    stage: "lang_to",
                    originalSlides: originalSlides,
                    translatedSlides: translatedSlides,
                  },
                }
              )
                .then(() => {
                  return resolve({
                    textMessages: [translationMessages.langToQ],
                    to: msg.sender,
                  });
                })
                .catch(reject);
            })
            .catch(reject);
        }
        return resolve({
          textMessages: [translationMessages.invalidLang],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

module.exports = handleLangFromStage;
