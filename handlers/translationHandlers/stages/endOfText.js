const Conversation = require("../../../models/Conversation");
const Translation = require("../../../models/Translation");
const { translationMessages, isoLangs } = require("../../utils/constants");

const handleEndOfTextTranslation = (msg, translation, conversation) => {
  return new Promise((resolve, reject) => {
    if (
      translation.translationType === "audio" ||
      translation.translationType === "text_audio"
    ) {
      return Translation.findOneAndUpdate(
        { _id: translation._id },
        {
          $set: {
            stage: "audio_translation",
            currentSlideNumber: 1,
          },
        },
        { new: true }
      )
        .then((updatedTranslation) => {
          const resolved = {
            videoUrl: updatedTranslation.originalSlides[0].videoUrl,
            videoIndex: 3,
            textMessages: [
              `${translationMessages.menuTip}\n\n${translationMessages.changeVideoTip}`,
              translationMessages.startAudioTranslation,
              `${translationMessages.voiceTip} ${Math.floor(
                updatedTranslation.originalSlides[0].duration
              )}`,
              `Slide 1/${updatedTranslation.originalSlides.length}\n\n${
                isoLangs[updatedTranslation.langFrom].name
              }:\n\n${updatedTranslation.originalSlides[0].text}`,
              `Maximum time: ${Math.floor(
                updatedTranslation.originalSlides[0].duration
              )} seconds`,
            ],
            to: msg.sender,
          };
          if (updatedTranslation.textTranslated) {
            resolved.textMessages[3] += `\n\n${
              isoLangs[updatedTranslation.langTo].name
            }:\n\n${updatedTranslation.translatedSlides[0].text}`;
          }
          return resolve(resolved);
        })
        .catch(reject);
    } else if (translation.translationType === "text") {
      return Translation.update(
        { _id: translation._id },
        {
          $set: {
            stage: "completed",
          },
        }
      )
        .then(() => {
          return Conversation.update(
            { _id: conversation._id },
            {
              $set: {
                stage: "completed",
              },
            }
          );
        })
        .then(() => {
          return resolve({
            textMessages: [translationMessages.success],
            to: msg.sender,
          });
        })

        .catch(reject);
    }
  });
};

module.exports = handleEndOfTextTranslation;
