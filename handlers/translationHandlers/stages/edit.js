const handleEditingRequest = require("./editRequest");
const handleEditingSlide = require("./editSlide");

const handleEditTranslation = (msg, translation, conversation) => {
  return new Promise((resolve, reject) => {
    if (translation.editingStage === "editing_request") {
      return handleEditingRequest(msg, translation).then(resolve).catch(reject);
    }
    if (translation.editingStage === "editing_slide") {
      return handleEditingSlide(msg, translation, conversation)
        .then(resolve)
        .catch(reject);
    }
  });
};

module.exports = handleEditTranslation;
