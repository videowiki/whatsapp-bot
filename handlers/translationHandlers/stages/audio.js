const fs = require("fs");
const path = require("path");
const { getMedia } = require("../../../turn.io");
const uuid = require("uuid").v4;
const musicMetadata = require("music-metadata");
const storageService = require("@videowiki/services/storage")(
  process.env.STORAGE_SERVICE_API_ROOT
);
const Translation = require("../../../models/Translation");
const {
  getFileDuration,
  getFileDurationFromBuffer,
} = require("../../utils/helpers");
const { isoLangs, translationMessages } = require("../../utils/constants");
const {
  translationAudioChanged,
} = require("../../../events/emitters/platformEvents");

const handleAudioTranslationStage = (msg, translation, conversation) => {
  return new Promise((resolve, reject) => {
    if (msg.type !== "voice") {
      resolve({
        textMessages: [translationMessages.invalidVoice],
        to: msg.sender,
      });
    }
    let originalDuration;
    return getFileDuration(
      translation.originalSlides[translation.currentSlideNumber - 1].audio
    )
      .then((od) => {
        originalDuration = od;
        return getMedia(msg.content);
      })
      .then((buffer) => {
        return getFileDurationFromBuffer(buffer);
      })
      .then((translationDuration) => {
        if (
          Number(translationDuration.toFixed(1)) >
          Number(originalDuration.toFixed(1))
        ) {
          return resolve({
            textMessages: [translationMessages.longVoice],
            to: msg.sender,
          });
        }
        const updates = {};
        translation.translatedSlides[translation.currentSlideNumber - 1].audio =
          msg.content;
        updates.translatedSlides = translation.translatedSlides;
        if (
          translation.currentSlideNumber >= translation.originalSlides.length
        ) {
          let updatedTranslation;
          let buffer;
          let filePath;
          let extension;
          updates.audioTranslated = true;
          updates.currentSlideNumber = 0;
          return Translation.findOneAndUpdate(
            { _id: translation._id },
            { $set: updates },
            { new: true }
          )
            .then((ut) => {
              updatedTranslation = ut;
              return getMedia(msg.content);
            })
            .then((bf) => {
              buffer = bf;
              return musicMetadata.parseBuffer(buffer);
            })
            .then((metadata) => {
              return new Promise((resolve, reject) => {
                extension = metadata.format.container.toLowerCase();
                filePath = path.join(__dirname, `${uuid()}.${extension}`);
                fs.writeFile(filePath, buffer, (err) => {
                  if (err) reject(err);
                  console.log("The file has been saved!");
                  return resolve(filePath);
                });
              });
            })
            .then((filePath) => {
              return storageService.saveFile(
                "whatsapp-bot/voices",
                filePath.split("/").pop(),
                fs.createReadStream(filePath)
              );
            })
            .then(({ url }) => {
              fs.unlink(filePath, (err) => {
                if (err) console.log("removed file", err);
              });
              const slidesLength = updatedTranslation.translatedSlides.length;
              translationAudioChanged({
                videoId: updatedTranslation.videoId,
                langTo: updatedTranslation.langTo,
                audioUrl: url,
                contactNumber: conversation.contactNumber,
                slidePosition:
                  updatedTranslation.translatedSlides[slidesLength - 1]
                    .slidePosition,
                subslidePosition:
                  updatedTranslation.translatedSlides[slidesLength - 1]
                    .subslidePosition,
                completed: true,
                action: updatedTranslation.action,
              });
              return resolve({
                textMessages: [
                  `${translationMessages.menuTip}\n\n${translationMessages.mistakeTip}\n\n${translationMessages.changeVideoTip}\n\n${translationMessages.confirmAudio}`,
                ],
                to: msg.sender,
              });
            })
            .catch(reject);
        }
        let updatedTranslation;
        let buffer;
        let filePath;
        let extension;
        updates.currentSlideNumber = translation.currentSlideNumber + 1;
        return Translation.findOneAndUpdate(
          { _id: translation._id },
          {
            $set: updates,
          },
          { new: true }
        )
          .then((ut) => {
            updatedTranslation = ut;
            return getMedia(msg.content);
          })
          .then((bf) => {
            buffer = bf;
            return musicMetadata.parseBuffer(buffer);
          })
          .then((metadata) => {
            return new Promise((resolve, reject) => {
              extension = metadata.format.container.toLowerCase();
              filePath = path.join(__dirname, `${uuid()}.${extension}`);
              fs.writeFile(filePath, buffer, (err) => {
                if (err) reject(err);
                console.log("The file has been saved!");
                return resolve(filePath);
              });
            });
          })
          .then((filePath) => {
            return storageService.saveFile(
              "whatsapp-bot/voices",
              filePath.split("/").pop(),
              fs.createReadStream(filePath)
            );
          })
          .then(({ url }) => {
            fs.unlink(filePath, (err) => {
              if (err) console.log("removed file", err);
            });
            translationAudioChanged({
              videoId: updatedTranslation.videoId,
              langTo: updatedTranslation.langTo,
              audioUrl: url,
              contactNumber: conversation.contactNumber,
              slidePosition:
                updatedTranslation.translatedSlides[
                  updatedTranslation.currentSlideNumber - 2
                ].slidePosition,
              subslidePosition:
                updatedTranslation.translatedSlides[
                  updatedTranslation.currentSlideNumber - 2
                ].subslidePosition,
              completed: false,
              action: updatedTranslation.action,
            });
            const resolved = {
              videoUrl:
                updatedTranslation.originalSlides[
                  updatedTranslation.currentSlideNumber - 1
                ].videoUrl,
              videoIndex: 2,
              textMessages: [
                `${translationMessages.menuTip}\n\n${translationMessages.mistakeTip}\n\n${translationMessages.changeVideoTip}`,
                `${translationMessages.voiceTip} ${Math.floor(
                  updatedTranslation.originalSlides[
                    updatedTranslation.currentSlideNumber - 1
                  ].duration
                )}`,
                `Slide ${updatedTranslation.currentSlideNumber}/${
                  updatedTranslation.originalSlides.length
                }\n\n${isoLangs[updatedTranslation.langFrom].name}:\n\n${
                  updatedTranslation.originalSlides[
                    updatedTranslation.currentSlideNumber - 1
                  ].text
                }`,
                `Maximum time: ${Math.floor(
                  updatedTranslation.originalSlides[
                    updatedTranslation.currentSlideNumber - 1
                  ].duration
                )} seconds`,
              ],
              to: msg.sender,
            };
            if (updatedTranslation.textTranslated) {
              resolved.textMessages[2] += `\n\n${
                isoLangs[updatedTranslation.langTo].name
              }:\n\n${
                updatedTranslation.translatedSlides[
                  updatedTranslation.currentSlideNumber - 1
                ].text
              }`;
            }
            return resolve(resolved);
          })
          .catch(reject);
      })
      .catch(reject);
  });
};

module.exports = handleAudioTranslationStage;
