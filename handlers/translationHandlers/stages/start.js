const Translation = require("../../../models/Translation");
const { translationMessages, isoLangs } = require("../../utils/constants");
const {
  translationStarted,
} = require("../../../events/emitters/platformEvents");

const startTranslation = (msg, translation, type, langToCode) => {
  return new Promise((resolve, reject) => {
    const updates = {};
    updates.currentSlideNumber = 1;
    if (langToCode) updates.langTo = langToCode;
    translationStarted({
      videoId: translation.videoId,
      langTo: updates.langTo,
      contactNumber: msg.sender,
    });
    console.log(
      "emitting translation started from random",
      translation,
      msg.sender
    );

    if (type === "text") {
      // TEXT TYPE
      return Translation.update(
        { _id: translation._id },
        {
          $set: {
            stage: "text_translation",
            translationType: "text",
            ...updates,
          },
        }
      )
        .then(() => {
          resolve({
            videoUrl: translation.originalSlides[0].videoUrl,
            videoIndex: 2,
            textMessages: [
              `${translationMessages.menuTip}\n\n${translationMessages.changeVideoTip}`,
              translationMessages.startTextTranslation,
              `Slide 1/${translation.originalSlides.length}\n\n${
                isoLangs[translation.langFrom].name
              }:\n\n${translation.originalSlides[0].text}`,
            ],
            to: msg.sender,
          });
        })
        .catch(reject);
    }

    if (type === "audio") {
      // AUDIO_TYPE
      return Translation.update(
        { _id: translation._id },
        {
          $set: {
            stage: "audio_translation",
            translationType: "audio",
            ...updates,
          },
        }
      )
        .then(() => {
          resolve({
            videoUrl: translation.originalSlides[0].videoUrl,
            videoIndex: 3,
            textMessages: [
              `${translationMessages.menuTip}\n\n${translationMessages.changeVideoTip}`,
              translationMessages.startAudioTranslation,
              `${translationMessages.voiceTip} ${Math.floor(
                translation.originalSlides[0].duration
              )}`,
              `Slide 1/${translation.originalSlides.length}\n\n${
                isoLangs[translation.langFrom].name
              }:\n\n${translation.originalSlides[0].text}`,
              `Maximum time: ${Math.floor(
                translation.originalSlides[0].duration
              )} seconds`,
            ],
            to: msg.sender,
          });
        })
        .catch(reject);
    }

    if (type === "text_audio") {
      // TEXT_AUDIO
      return Translation.update(
        { _id: translation._id },
        {
          $set: {
            stage: "text_translation",
            translationType: "text_audio",
            ...updates,
          },
        }
      )
        .then(() => {
          resolve({
            videoUrl: translation.originalSlides[0].videoUrl,
            videoIndex: 2,
            textMessages: [
              `${translationMessages.menuTip}\n\n${translationMessages.changeVideoTip}`,
              translationMessages.startTextTranslation,
              `Slide 1/${translation.originalSlides.length}\n\n${
                isoLangs[translation.langFrom].name
              }:\n\n${translation.originalSlides[0].text}`,
            ],
            to: msg.sender,
          });
        })
        .catch(reject);
    }

    return resolve({
      textMessages: [translationMessages.invalidInput],
      to: msg.sender,
    });
  });
};

module.exports = startTranslation;
