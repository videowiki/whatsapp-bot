const Translation = require("../../../models/Translation");
const { fetchTranslationArticle } = require("../../utils/api");
const { translationMessages, isoLangs } = require("../../utils/constants");
const { getTranslationSlides } = require("../../utils/helpers");

const handleChangeVideo = (msg, translation) => {
  return new Promise((resolve, reject) => {
    fetchTranslationArticle(translation.langFrom)
      .then((article) => {
        const { originalSlides, translatedSlides } = getTranslationSlides(
          article
        );
        const updates = {
          articleId: article._id,
          originalSlides: originalSlides,
          translatedSlides: translatedSlides,
          currentSlideNumber: 1,
          textTranslated: false,
          audioTranslated: false,
          editing: false,
          editingStage: "pending",
          editingSlideNumber: undefined,
        };
        if (
          translation.translationType === "text" ||
          translation.translationType === "text_audio"
        ) {
          updates.stage = "text_translation";
        } else if (translation.translationType === "audio") {
          updates.stage = "audio_translation";
        }
        return Translation.findOneAndUpdate(
          { _id: translation._id },
          {
            $set: updates,
          },
          { new: true }
        );
      })
      .then((translation) => {
        if (
          translation.translationType === "text" ||
          translation.translationType === "text_audio"
        ) {
          return resolve({
            videoUrl: translation.originalSlides[0].videoUrl,
            videoIndex: 2,
            textMessages: [
              `${translationMessages.menuTip}\n\n${translationMessages.changeVideoTip}`,
              translationMessages.startTextTranslation,
              `Slide 1/${translation.originalSlides.length}\n\n${
                isoLangs[translation.langFrom].name
              }:\n\n${translation.originalSlides[0].text}`,
            ],
            to: msg.sender,
          });
        } else if (translation.translationType === "audio") {
          return resolve({
            videoUrl: translation.originalSlides[0].videoUrl,
            videoIndex: 3,
            textMessages: [
              `${translationMessages.menuTip}\n\n${translationMessages.changeVideoTip}`,
              translationMessages.startAudioTranslation,
              `${translationMessages.voiceTip} ${Math.floor(
                translation.originalSlides[0].duration
              )}`,
              `Slide 1/${translation.originalSlides.length}\n\n${
                isoLangs[translation.langFrom].name
              }:\n\n${translation.originalSlides[0].text}`,
              `Maximum time: ${Math.floor(
                translation.originalSlides[0].duration
              )} seconds`,
            ],
            to: msg.sender,
          });
        }
      })
      .catch(reject);
  });
};

module.exports = handleChangeVideo;
