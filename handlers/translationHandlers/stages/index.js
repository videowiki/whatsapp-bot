const handleTranslationTypeStage = require("./type");
const handleLangFromStage = require("./langFrom");
const handleLangToStage = require("./langTo");
const handleStartedTranslationOptions = require("./startedTranslationOptions");

const handleTranslationStages = (msg, translation, conversation) => {
  return new Promise((resolve, reject) => {
    if (translation.stage === "translation_type") {
      return handleTranslationTypeStage(msg, translation)
        .then(resolve)
        .catch(reject);
    }
    if (translation.stage === "lang_from") {
      return handleLangFromStage(msg, translation).then(resolve).catch(reject);
    }
    if (translation.stage === "lang_to") {
      return handleLangToStage(msg, translation).then(resolve).catch(reject);
    }
    if (
      translation.stage === "text_translation" ||
      translation.stage === "audio_translation"
    ) {
      return handleStartedTranslationOptions(msg, translation, conversation)
        .then(resolve)
        .catch(reject);
    }
  });
};

module.exports = handleTranslationStages;
