const Conversation = require("../../../models/Conversation");
const Translation = require("../../../models/Translation");
const { translationMessages } = require("../../utils/constants");

const handleTranslationSelect = (msg, conversation, translationType) => {
  return new Promise((resolve, reject) => {
    return Translation.create({
      stage: "lang_from",
      translationType,
    })
      .then((translation) => {
        return Conversation.update(
          { _id: conversation._id },
          {
            $set: {
              translation: translation._id,
              stage: "started",
              type: "translation",
            },
          }
        );
      })
      .then(() => {
        resolve({
          textMessages: [translationMessages.langFromQ],
          to: msg.sender,
        });
      })

      .catch(reject);
  });
};

module.exports = handleTranslationSelect;
