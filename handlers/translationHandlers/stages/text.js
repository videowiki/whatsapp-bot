const stringSimilarity = require("string-similarity");
const Translation = require("../../../models/Translation");
const { detectLang, translateText } = require("../../utils/helpers");
const { isoLangs, translationMessages } = require("../../utils/constants");
const {
  translationTextChanged,
} = require("../../../events/emitters/platformEvents");

const handleTextTranslationStage = (msg, translation, conversation) => {
  return new Promise((resolve, reject) => {
    if (msg.type !== "text") {
      return resolve({
        textMessages: [translationMessages.invalidText],
        to: msg.sender,
      });
    }
    return detectLang(msg.content)
      .then((langCode) => {
        if (translation.langTo === langCode) {
          return translateText(
            translation.originalSlides[translation.currentSlideNumber - 1].text,
            translation.langTo
          )
            .then((translatedText) => {
              const similarity = stringSimilarity.compareTwoStrings(
                msg.content,
                translatedText
              );
              if (similarity < 0.1) {
                return resolve({
                  textMessages: [translationMessages.spamError],
                  to: msg.sender,
                });
              }
              if (similarity < 0.2) {
                return resolve({
                  textMessages: [translationMessages.spamWarning],
                  to: msg.sender,
                });
              }
              if (similarity >= 0.2) {
                translation.translatedSlides[
                  translation.currentSlideNumber - 1
                ].text = msg.content;
                if (
                  translation.currentSlideNumber >=
                  translation.originalSlides.length
                ) {
                  return Translation.findOneAndUpdate(
                    { _id: translation._id },
                    {
                      $set: {
                        textTranslated: true,
                        currentSlideNumber: 0,
                        translatedSlides: translation.translatedSlides,
                      },
                    },
                    { new: true }
                  )
                    .then((updatedTranslation) => {
                      const slidesLength =
                        updatedTranslation.translatedSlides.length;
                      translationTextChanged({
                        videoId: updatedTranslation.videoId,
                        langTo: updatedTranslation.langTo,
                        text:
                          updatedTranslation.translatedSlides[slidesLength - 1]
                            .text,
                        contactNumber: conversation.contactNumber,
                        slidePosition:
                          updatedTranslation.translatedSlides[slidesLength - 1]
                            .slidePosition,
                        subslidePosition:
                          updatedTranslation.translatedSlides[slidesLength - 1]
                            .subslidePosition,
                        completed: true,
                        action: updatedTranslation.action,
                      });
                      return resolve({
                        textMessages: [
                          `${translationMessages.menuTip}\n\n${translationMessages.mistakeTip}\n\n${translationMessages.changeVideoTip}\n\n${translationMessages.confirmText}`,
                        ],
                        to: msg.sender,
                      });
                    })
                    .catch(reject);
                }
                return Translation.findOneAndUpdate(
                  { _id: translation._id },
                  {
                    $set: {
                      currentSlideNumber: translation.currentSlideNumber + 1,
                      translatedSlides: translation.translatedSlides,
                    },
                  },
                  { new: true }
                )
                  .then((updatedTranslation) => {
                    translationTextChanged({
                      videoId: updatedTranslation.videoId,
                      langTo: updatedTranslation.langTo,
                      text:
                        updatedTranslation.translatedSlides[
                          updatedTranslation.currentSlideNumber - 2
                        ].text,
                      contactNumber: conversation.contactNumber,
                      slidePosition:
                        updatedTranslation.translatedSlides[
                          updatedTranslation.currentSlideNumber - 2
                        ].slidePosition,
                      subslidePosition:
                        updatedTranslation.translatedSlides[
                          updatedTranslation.currentSlideNumber - 2
                        ].subslidePosition,
                      completed: false,
                      action: updatedTranslation.action,
                    });
                    return resolve({
                      videoUrl:
                        updatedTranslation.originalSlides[
                          updatedTranslation.currentSlideNumber - 1
                        ].videoUrl,
                      videoIndex: 1,
                      textMessages: [
                        `${translationMessages.menuTip}\n\n${translationMessages.mistakeTip}\n\n${translationMessages.changeVideoTip}`,
                        `Slide ${updatedTranslation.currentSlideNumber}/${
                          updatedTranslation.originalSlides.length
                        }\n\n${
                          isoLangs[updatedTranslation.langFrom].name
                        }:\n\n${
                          updatedTranslation.originalSlides[
                            updatedTranslation.currentSlideNumber - 1
                          ].text
                        }`,
                      ],
                      to: msg.sender,
                    });
                  })
                  .catch(reject);
              }
            })
            .catch(reject);
        }
        return resolve({
          textMessages: [
            translationMessages.langDetectionError.replace(
              "%l%",
              isoLangs[translation.langTo].name
            ),
          ],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

module.exports = handleTextTranslationStage;
