const Translation = require("../../../models/Translation");
const { isoLangs, translationMessages } = require("../../utils/constants");

const handleEditingRequest = (msg, translation) => {
  return new Promise((resolve, reject) => {
    const slideNumber = Number(
      msg.content.replace("Slide ", "").replace("slide ", "").replace(".", "")
    );
    if (
      slideNumber > 0 &&
      slideNumber <= translation.originalSlides.length &&
      ((translation.stage === "text_translation" &&
        translation.translatedSlides[slideNumber - 1].text) ||
        (translation.stage === "audio_translation" &&
          translation.translatedSlides[slideNumber - 1].audio))
    ) {
      return Translation.findOneAndUpdate(
        { _id: translation._id },
        {
          $set: {
            editingStage: "editing_slide",
            editingSlideNumber: slideNumber,
          },
        },
        { new: true }
      )
        .then((updatedTranslation) => {
          if (updatedTranslation.stage === "text_translation") {
            return resolve({
              videoUrl:
                updatedTranslation.originalSlides[
                  updatedTranslation.editingSlideNumber - 1
                ].videoUrl,
              videoIndex: 0,
              textMessages: [
                `Slide ${slideNumber}/${translation.originalSlides.length}\n\n${
                  isoLangs[updatedTranslation.langFrom].name
                }:\n\n${translation.originalSlides[slideNumber - 1].text}\n\n${
                  isoLangs[updatedTranslation.langTo].name
                }:\n\n${translation.translatedSlides[slideNumber - 1].text}`,
              ],
              to: msg.sender,
            });
          }
          if (updatedTranslation.stage === "audio_translation") {
            const resolved = {
              videoUrl:
                updatedTranslation.originalSlides[slideNumber - 1].videoUrl,
              videoIndex: 1,
              textMessages: [
                `${translationMessages.voiceTip} ${Math.floor(
                  updatedTranslation.originalSlides[slideNumber - 1].duration
                )}`,
                `Slide ${slideNumber}/${
                  updatedTranslation.originalSlides.length
                }\n\n${isoLangs[updatedTranslation.langFrom].name}:\n\n${
                  updatedTranslation.originalSlides[slideNumber - 1].text
                }`,
                `Maximum time: ${Math.floor(
                  updatedTranslation.originalSlides[slideNumber - 1].duration
                )} seconds`,
              ],
              to: msg.sender,
            };
            if (updatedTranslation.textTranslated) {
              resolved.textMessages[1] += `\n\n${
                isoLangs[updatedTranslation.langTo].name
              }:\n\n${
                updatedTranslation.translatedSlides[slideNumber - 1].text
              }`;
            }
            return resolve(resolved);
          }
        })
        .catch(reject);
    }
    return resolve({
      textMessages: [translationMessages.invalidEditing],
      to: msg.sender,
    });
  });
};

module.exports = handleEditingRequest;
