const Translation = require("../../../models/Translation");
const { translationMessages } = require("../../utils/constants");

const handleMistake = (msg, translation) => {
  return new Promise((resolve, reject) => {
    return Translation.update(
      { _id: translation._id },
      {
        $set: {
          editing: true,
          editingStage: "editing_request",
        },
      }
    )
      .then(() => {
        return resolve({
          textMessages: [translationMessages.editing],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

module.exports = handleMistake;
