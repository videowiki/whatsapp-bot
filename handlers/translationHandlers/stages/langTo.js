const startTranslation = require("./start");
const { getLangCode, getLangInEnglish } = require("../../utils/helpers");
const { translationMessages } = require("../../utils/constants");

const handleLangToStage = (msg, translation) => {
  return new Promise((resolve, reject) => {
    const nativeLangCode = getLangCode(msg.content);
    if (nativeLangCode) {
      return startTranslation(
        msg,
        translation,
        translation.translationType,
        nativeLangCode
      )
        .then(resolve)
        .catch(reject);
    }
    return getLangInEnglish(msg.content)
      .then((langName) => {
        const code = getLangCode(langName);
        if (code) {
          return startTranslation(
            msg,
            translation,
            translation.translationType,
            code
          )
            .then(resolve)
            .catch(reject);
        }
        return resolve({
          textMessages: [translationMessages.invalidLang],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

module.exports = handleLangToStage;
