const Conversation = require("../../../models/Conversation");
const Translation = require("../../../models/Translation");
const { fetchVideoById, fetchArticleById } = require("../../utils/api");
const { getTranslationSlides } = require("../../utils/helpers");
const {
  translationStarted,
} = require("../../../events/emitters/platformEvents");

const handlePlatformTranslationAction = (msg, videoId, langTo) => {
  return new Promise((resolve, reject) => {
    let translation;
    return fetchVideoById(videoId)
      .then((video) => {
        if (video && video.article && video.status === "done") {
          return fetchArticleById(video.article);
        }
        return resolve({
          textMessages: ["The videos must be waiting for translation"],
          to: msg.sender,
        });
      })
      .then((article) => {
        const { originalSlides, translatedSlides } = getTranslationSlides(
          article
        );
        return Translation.create({
          videoId,
          articleId: article._id,
          stage: "translation_type",
          originalSlides,
          translatedSlides,
          langFrom: article.langCode.split("-")[0],
          langTo: langTo.split("-")[0],
          action: true,
        });
      })
      .then((t) => {
        translation = t;
        return Conversation.create({
          contactNumber: msg.sender,
          stage: "started",
          type: "translation",
          translation: t._id,
        });
      })
      .then((conversation) => {
        translationStarted({
          videoId,
          langTo: translation.langTo,
          contactNumber: conversation.contactNumber,
          action: true,
        });
        return resolve({
          textMessages: [
            `Please select the translation type \n\n *1.* Translate text \n\n *2.* Add voice-overs \n\n *3.* Translate text and add voice-overs`,
          ],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

module.exports = handlePlatformTranslationAction;
