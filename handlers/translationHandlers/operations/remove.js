const Conversation = require("../../../models/Conversation");
const handleBreakingSelect = require("../../transcriptionHandlers/stages/breaking/select");
const handleTranscriptionSelect = require("../../transcriptionHandlers/stages/transcribing/select");
const handleSubtitlesSelect = require("../../subtitlesHandlers/stages/select");
const handleVerificationSelect = require("../../verificationHandlers/stages/select");

const removeTranslation = (msg, translation, conversation, newModel) => {
  return new Promise((resolve, reject) => {
    translation
      .remove()
      .then(() => {
        return Conversation.update(
          { _id: conversation._id },
          {
            $set: {
              stage: "option_select",
              type: null,
              translation: null,
            },
          }
        );
      })
      .then(() => {
        if (newModel === "BrokenVideo") {
          return handleBreakingSelect(msg, conversation)
            .then(resolve)
            .catch(reject);
        }
        if (newModel === "Transcription") {
          return handleTranscriptionSelect(msg, conversation)
            .then(resolve)
            .catch(reject);
        }
        if (newModel === "Subtitle") {
          return handleSubtitlesSelect(msg, conversation)
            .then(resolve)
            .catch(reject);
        }
        if (newModel === "Verification") {
          return handleVerificationSelect(msg).then(resolve).catch(reject);
        }
      })
      .catch(reject);
  });
};

module.exports = removeTranslation;
