const Translation = require("../../../models/Translation");
const { translationMessages } = require("../../utils/constants");

const resetTranslation = (msg, translation, type) => {
  return new Promise((resolve, reject) => {
    Translation.update(
      { _id: translation._id },
      {
        $set: {
          stage: "lang_from",
          translationType: type,
          articleId: undefined,
          originalSlides: [],
          translatedSlides: [],
          currentSlideNumber: undefined,
          textTranslated: false,
          audioTranslated: false,
          langFrom: undefined,
          langTo: undefined,
          editing: false,
          editingStage: 'pending',
          editingSlideNumber: undefined,
        },
      }
    )
      .then(() => {
        resolve({
          textMessages: [translationMessages.langFromQ],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

module.exports = resetTranslation;
