const BrokenVideo = require("../../../models/BrokenVideo");
const { fetchVideoById } = require("../../utils/api");
const { WHITELISTED_NUMBERS } = require("../../utils/constants");
const { sendToUser } = require("../../../events/emitters/botEvents");
const {
  videoPendingBreaking,
} = require("../../../events/emitters/platformEvents");

const handlePlatformBreakingAction = (msg, videoId) => {
  return new Promise((resolve, reject) => {
    fetchVideoById(videoId)
      .then((video) => {
        if (video && video.status === "uploaded") {
          return BrokenVideo.create({
            videoId: video._id,
            title: video.title,
            videoUrl: video.url,
            duration: Math.floor(video.duration),
            numberOfSpeakers: video.numberOfSpeakers,
            stage: "pending_breaking",
            langCode: video.langCode,
            requestedBy: msg.sender,
          })
            .then(() => {
              WHITELISTED_NUMBERS.forEach((number) => {
                sendToUser({
                  to: number,
                  textMessages: ["New video has been uploaded to be broken"],
                });
              });
              videoPendingBreaking({ videoId: video._id });
              resolve({
                textMessages: [
                  "We will notify you when your video is ready for stage 2",
                ],
                to: msg.sender,
              });
            })
            .catch(reject);
        }
        return resolve({
          textMessages: ["The video must be in the uploaded stage"],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

module.exports = handlePlatformBreakingAction;
