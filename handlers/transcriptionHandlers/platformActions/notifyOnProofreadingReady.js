const UserActionSubscription = require("../../../models/UserActionSubscription");
const videoService = require("@videowiki/services/video")(
  process.env.VIDEO_SERVICE_API_ROOT
);
const handlePlatformNotifyOnProofreadingReadyAction = (msg, videoId) => {
  return new Promise((resolve, reject) => {
    let video;
    return videoService
      .findById(videoId)
      .then((v) => {
        video = v;
        if (!video || video.status !== "cutting") {
          return resolve({
            textMessages: ["The video must be in the cutting stage"],
            to: msg.sender,
          });
        }
        return UserActionSubscription.findOne({
          type: "proofreading_ready",
          resourceId: videoId,
        });
      })
      .then((subscription) => {
        if (!subscription) {
          return UserActionSubscription.create({
            type: "proofreading_ready",
            resourceId: videoId,
            contactNumbers: [msg.sender],
          });
        }
        return UserActionSubscription.findOneAndUpdate(
          {
            type: "proofreading_ready",
            resourceId: videoId,
          },
          { $addToSet: { contactNumbers: msg.sender } }
        );
      })
      .then(() => {
        return resolve({
          textMessages: [
            `We will notify you when the video *"${video.title}"* is ready for proofreading`,
          ],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

module.exports = handlePlatformNotifyOnProofreadingReadyAction;
