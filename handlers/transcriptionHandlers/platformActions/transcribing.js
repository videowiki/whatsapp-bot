const Conversation = require("../../../models/Conversation");
const BrokenVideo = require("../../../models/BrokenVideo");
const Transcription = require("../../../models/Transcription");
const { fetchVideoById, fetchArticleById } = require("../../utils/api");
const {
  cutVideo,
  getNotSilentSlidePosition,
  getTranscriptionSlides,
} = require("../../utils/helpers");

const handlePlatformTranscribingAction = (msg, videoId) => {
  return new Promise((resolve, reject) => {
    BrokenVideo.findOne({ videoId: videoId, stage: "pending_transcribing" })
      .then((brokenVideo) => {
        return new Promise((resolve, reject) => {
          if (brokenVideo) return resolve(brokenVideo);
          let video;
          let article;
          fetchVideoById(videoId)
            .then((v) => {
              video = v;
              if (!video || video.status !== "proofreading") {
                const err = new Error(
                  "The video must be in the proofreading stage"
                );
                err.userFacing = true;
                throw err;
              }
              return fetchArticleById(video.article);
            })
            .then((a) => {
              article = a;
              return BrokenVideo.create({
                videoId: video._id,
                title: video.title,
                videoUrl: video.url,
                duration: Math.floor(video.duration),
                numberOfSpeakers: video.numberOfSpeakers,
                stage: "pending_transcribing",
                langCode: video.langCode,
                slides: getTranscriptionSlides(article),
              });
            })
            .then(resolve)
            .catch(reject);
        });
      })
      .catch((err) => {
        if (err.userFacing) {
          return resolve({
            textMessages: [err.message],
            to: msg.sender,
          });
        }
        reject(err);
      })
      .then((brokenVideo) => {
        let transcription;
        const slides = brokenVideo.slides;
        const notSilentPosition = getNotSilentSlidePosition(slides, 0);
        return Transcription.create({
          videoId: brokenVideo.videoId,
          videoUrl: brokenVideo.videoUrl,
          langCode: brokenVideo.langCode,
          currentSlideNumber: notSilentPosition + 1,
          stage: "transcribing",
          slides,
          brokenVideo: brokenVideo._id,
        })
          .then((t) => {
            transcription = t;
            return Conversation.create({
              contactNumber: msg.sender,
              stage: "started",
              type: "transcription",
              transcription: transcription._id,
            });
          })
          .then(() => {
            return cutVideo(
              transcription.videoUrl,
              transcription.slides[transcription.currentSlideNumber - 1]
                .startTime,
              transcription.slides[transcription.currentSlideNumber - 1]
                .endTime -
                transcription.slides[transcription.currentSlideNumber - 1]
                  .startTime
            );
          })
          .then((videoPath) => {
            return resolve({
              videoUrl:
                "https://tailoredvideowiki.s3-eu-west-1.amazonaws.com/static/tutorials_media/whatsapp_tutorials/stage2_compressed.mp4",
              videoPath,
              videoIndex: 1,
              textMessages: [
                "Here's a quick tutorial for how to transcribe video on WhatsApp:",
                `Type the audio you hear in slide ${transcription.currentSlideNumber}/${transcription.slides.length}`,
              ],
              to: msg.sender,
            });
          })
          .catch(reject);
      })
      .catch(reject);
  });
};

module.exports = handlePlatformTranscribingAction;
