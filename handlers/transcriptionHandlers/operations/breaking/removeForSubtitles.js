const Conversation = require("../../../../models/Conversation");
const BrokenVideo = require("../../../../models/BrokenVideo");
const handleSubtitlesSelect = require("../../../subtitlesHandlers/stages/select");

const removeBreakingForSubtitles = (msg, brokenVideo, conversation) => {
  return new Promise((resolve, reject) => {
    return BrokenVideo.update(
      { _id: brokenVideo._id },
      {
        $set: {
          stage: "pending_breaking",
          currentSlideNumber: 0,
          slides: [],
        },
      }
    )
      .then(() => {
        return Conversation.update(
          { _id: conversation._id },
          {
            $set: {
              stage: "option_select",
              brokenVideo: null,
              type: null,
            },
          }
        );
      })
      .then(() => {
        return handleSubtitlesSelect(msg, conversation)
          .then(resolve)
          .catch(reject);
      })
      .catch(reject);
  });
};

module.exports = removeBreakingForSubtitles;
