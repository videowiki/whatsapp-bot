const Conversation = require("../../../../models/Conversation");
const Translation = require("../../../../models/Translation");
const BrokenVideo = require("../../../../models/BrokenVideo");
const { translationMessages } = require("../../../utils/constants");

const removeBreakingForTranslation = (
  msg,
  brokenVideo,
  conversation,
  translationType
) => {
  return new Promise((resolve, reject) => {
    return BrokenVideo.update(
      { _id: brokenVideo._id },
      {
        $set: {
          stage: "pending_breaking",
          currentSlideNumber: 0,
          slides: [],
        },
      }
    )
      .then(() => {
        return Translation.create({
          stage: "lang_from",
          translationType,
        });
      })
      .then((translation) => {
        return Conversation.update(
          { _id: conversation._id },
          {
            $set: {
              brokenVideo: null,
              translation: translation._id,
              stage: "started",
              type: "translation",
            },
          }
        );
      })
      .then(() => {
        return resolve({
          textMessages: [translationMessages.langFromQ],
          to: msg.sender,
        });
      })

      .catch(reject);
  });
};

module.exports = removeBreakingForTranslation;
