const Conversation = require("../../../../models/Conversation");
const Transcription = require("../../../../models/Transcription");
const BrokenVideo = require("../../../../models/BrokenVideo");

const removeBreakingForTranscription = (msg, brokenVideo, conversation) => {
  return new Promise((resolve, reject) => {
    return BrokenVideo.update(
      { _id: brokenVideo._id },
      {
        $set: {
          stage: "pending_breaking",
          currentSlideNumber: 0,
          slides: [],
        },
      }
    )
      .then(() => {
        return Transcription.create({
          stage: "lang_select",
        });
      })
      .then((transcription) => {
        return Conversation.update(
          { _id: conversation._id },
          {
            $set: {
              brokenVideo: null,
              transcription: transcription._id,
              stage: "started",
              type: "transcription",
            },
          }
        );
      })
      .then(() => {
        return resolve({
          textMessages: [
            "Which language do you know? (Write one language only)",
          ],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

module.exports = removeBreakingForTranscription;
