const Conversation = require("../../../../models/Conversation");
const BrokenVideo = require("../../../../models/BrokenVideo");
const handleVerificationSelect = require("../../../verificationHandlers/stages/select");

const removeBreakingForVerification = (msg, brokenVideo, conversation) => {
  return new Promise((resolve, reject) => {
    return BrokenVideo.update(
      { _id: brokenVideo._id },
      {
        $set: {
          stage: "pending_breaking",
          currentSlideNumber: 0,
          slides: [],
        },
      }
    )
      .then(() => {
        return Conversation.update(
          { _id: conversation._id },
          {
            $set: {
              stage: "option_select",
              brokenVideo: null,
              type: null,
            },
          }
        );
      })
      .then(() => {
        return handleVerificationSelect(msg).then(resolve).catch(reject);
      })
      .catch(reject);
  });
};

module.exports = removeBreakingForVerification;
