const BrokenVideo = require("../../../../models/BrokenVideo");
const handleBreakingSelect = require("../../stages/breaking/select");

const resetBreaking = (msg, brokenVideo, conversation) => {
  return new Promise((resolve, reject) => {
    handleBreakingSelect(msg, conversation)
      .then((response) => {
        BrokenVideo.update(
          { _id: brokenVideo._id },
          {
            $set: {
              stage: "pending_breaking",
              currentSlideNumber: 0,
              slides: [],
            },
          }
        )
          .then(() => {
            resolve(response);
          })
          .catch(reject);
      })
      .catch(reject);
  });
};

module.exports = resetBreaking;
