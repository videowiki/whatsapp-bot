const Translation = require("../../../../models/Translation");
const Conversation = require("../../../../models/Conversation");
const { translationMessages } = require("../../../utils/constants");

const removeTranscriptionForTranslation = (
  msg,
  transcription,
  conversation,
  translationType
) => {
  return new Promise((resolve, reject) => {
    transcription
      .deleteOne({ _id: transcription._id })
      .then(() => {
        return Translation.create({
          stage: "lang_from",
          translationType,
        });
      })
      .then((translation) => {
        return Conversation.update(
          { _id: conversation._id },
          {
            $set: {
              transcription: null,
              translation: translation._id,
              stage: "started",
              type: "translation",
            },
          }
        );
      })
      .then(() => {
        return resolve({
          textMessages: [translationMessages.langFromQ],
          to: msg.sender,
        });
      })

      .catch(reject);
  });
};

module.exports = removeTranscriptionForTranslation;
