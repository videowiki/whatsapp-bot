const Conversation = require("../../../../models/Conversation");
const Transcription = require("../../../../models/Transcription");
const handleVerificationSelect = require("../../../verificationHandlers/stages/select");

const removeTranscriptionForVerification = (
  msg,
  transcription,
  conversation
) => {
  return new Promise((resolve, reject) => {
    return Transcription.deleteOne({ _id: transcription._id })
      .then(() => {
        return Conversation.update(
          { _id: conversation._id },
          {
            $set: {
              stage: "option_select",
              transcription: null,
              type: null,
            },
          }
        );
      })
      .then(() => {
        return handleVerificationSelect(msg).then(resolve).catch(reject);
      })
      .catch(reject);
  });
};

module.exports = removeTranscriptionForVerification;
