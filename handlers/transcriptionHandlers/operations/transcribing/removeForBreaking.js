const Conversation = require("../../../../models/Conversation");
const Transcription = require("../../../../models/Transcription");
const handleBreakingSelect = require("../../stages/breaking/select");

const removeTranscriptionForBreaking = (msg, transcription, conversation) => {
  return new Promise((resolve, reject) => {
    return Transcription.deleteOne({ _id: transcription._id })
      .then(() => {
        return Conversation.updateOne(
          { _id: conversation._id },
          {
            $set: {
              transcription: null,
            },
          }
        );
      })
      .then(() => {
        handleBreakingSelect(msg, conversation).then(resolve).catch(reject);
      })
      .catch(reject);
  });
};

module.exports = removeTranscriptionForBreaking;
