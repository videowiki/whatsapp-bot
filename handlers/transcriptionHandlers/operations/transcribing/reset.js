const Transcription = require("../../../../models/Transcription");

const resetTranscription = (msg, transcription) => {
  return new Promise((resolve, reject) => {
    Transcription.update(
      { _id: transcription._id },
      {
        $set: {
          brokenVideo: null,
          videoId: null,
          videoUrl: null,
          langCode: null,
          stage: "lang_select",
          slides: [],
        },
      }
    )
      .then(() => {
        resolve({
          textMessages: [
            "Which language do you know? (Write one language only)",
          ],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

module.exports = resetTranscription;
