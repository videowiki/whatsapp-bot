const Conversation = require("../../../../models/Conversation");
const Transcription = require("../../../../models/Transcription");
const handleSubtitlesSelect = require("../../../subtitlesHandlers/stages/select");

const removeTranscriptionForSubtitles = (msg, transcription, conversation) => {
  return new Promise((resolve, reject) => {
    Transcription.deleteOne({ _id: transcription._id })
      .then(() => {
        return Conversation.update(
          { _id: conversation._id },
          {
            $set: {
              stage: "option_select",
              transcription: null,
              type: null,
            },
          }
        );
      })
      .then(() => {
        return handleSubtitlesSelect(msg, conversation)
          .then(resolve)
          .catch(reject);
      })
      .catch(reject);
  });
};

module.exports = removeTranscriptionForSubtitles;
