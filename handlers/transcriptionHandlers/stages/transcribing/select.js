const Conversation = require("../../../../models/Conversation");
const Transcription = require("../../../../models/Transcription");

const handleTranscriptionSelect = (msg, conversation) => {
  return new Promise((resolve, reject) => {
    Transcription.create({
      stage: "lang_select",
    })
      .then((transcription) => {
        return Conversation.update(
          { _id: conversation._id },
          {
            $set: {
              transcription: transcription._id,
              stage: "started",
              type: "transcription",
            },
          }
        );
      })
      .then(() => {
        resolve({
          textMessages: [
            "Which language do you know? (Write one language only)",
          ],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

module.exports = handleTranscriptionSelect;
