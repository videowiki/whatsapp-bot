const BrokenVideo = require("../../../../models/BrokenVideo");
const Transcription = require("../../../../models/Transcription");
const {
  getLangInEnglish,
  getLangCode,
  cutVideo,
  getNotSilentSlidePosition,
} = require("../../../utils/helpers");

const handleLangSelectStage = (msg, transcription) => {
  return new Promise((resolve, reject) => {
    const nativeLangCode = getLangCode(msg.content);
    if (nativeLangCode) {
      storeTranscriptionInfo(msg, transcription, nativeLangCode)
        .then(resolve)
        .catch(reject);
    } else {
      getLangInEnglish(msg.content)
        .then((langName) => {
          const langCode = getLangCode(langName);
          if (langCode) {
            storeTranscriptionInfo(transcription, langCode)
              .then(resolve)
              .catch(reject);
          } else {
            resolve({
              textMessages: ["Please send a valid language"],
              to: msg.sender,
            });
          }
        })
        .catch(reject);
    }
  });
};

const storeTranscriptionInfo = (msg, transcription, langCode) => {
  let updatedTranscription;
  return new Promise((resolve, reject) => {
    const langCodeRegex = new RegExp(`^${langCode}`);
    BrokenVideo.findOne({
      stage: "pending_transcribing",
      langCode: { $regex: langCodeRegex },
    })
      .then((brokenVideo) => {
        if (brokenVideo) {
          const notSilentPosition = getNotSilentSlidePosition(
            brokenVideo.slides,
            0
          );
          return Transcription.findOneAndUpdate(
            { _id: transcription._id },
            {
              $set: {
                brokenVideo: brokenVideo._id,
                videoId: brokenVideo.videoId,
                videoUrl: brokenVideo.videoUrl,
                langCode: brokenVideo.langCode,
                currentSlideNumber: notSilentPosition + 1,
                stage: "transcribing",
                slides: brokenVideo.slides,
              },
            },
            { new: true }
          )
            .then((t) => {
              updatedTranscription = t;
              return cutVideo(
                updatedTranscription.videoUrl,
                updatedTranscription.slides[
                  updatedTranscription.currentSlideNumber - 1
                ].startTime,
                updatedTranscription.slides[
                  updatedTranscription.currentSlideNumber - 1
                ].endTime -
                  updatedTranscription.slides[
                    updatedTranscription.currentSlideNumber - 1
                  ].startTime
              );
            })
            .then((videoPath) => {
              resolve({
                videoPath,
                videoIndex: 0,
                textMessages: [
                  `Type the audio you hear in slide ${updatedTranscription.currentSlideNumber}/${updatedTranscription.slides.length}`,
                ],
                to: msg.sender,
              });
            })
            .catch(reject);
        } else {
          resolve({
            textMessages: ["There is no videos available for transcribing, You can select another language"],
            to: msg.sender,
          });
        }
      })
      .catch(reject);
  });
};

module.exports = handleLangSelectStage;
