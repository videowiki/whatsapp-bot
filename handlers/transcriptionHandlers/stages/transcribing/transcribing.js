const Conversation = require("../../../../models/Conversation");
const BrokenVideo = require("../../../../models/BrokenVideo");
const Transcription = require("../../../../models/Transcription");
const {
  getNotSilentSlidePosition,
  cutVideo,
} = require("../../../utils/helpers");
const {
  videoTranscribed,
} = require("../../../../events/emitters/platformEvents");

const handleTranscribingStage = (msg, transcription, conversation) => {
  return new Promise((resolve, reject) => {
    const updates = {};
    const notSilentPosition = getNotSilentSlidePosition(
      transcription.slides,
      transcription.currentSlideNumber
    );
    transcription.slides[transcription.currentSlideNumber - 1].text =
      msg.content;
    updates.slides = transcription.slides;
    if (transcription.currentSlideNumber >= transcription.slides.length) {
      updates.stage = "completed";
      updates.currentSlideNumber = 0;
    } else {
      updates.currentSlideNumber = notSilentPosition + 1;
    }
    Transcription.findOneAndUpdate(
      { _id: transcription._id },
      { $set: updates },
      { new: true }
    )
      .then((updatedTranscription) => {
        if (updatedTranscription.stage === "completed") {
          return BrokenVideo.findOneAndUpdate(
            { _id: updatedTranscription.brokenVideo },
            { $set: { stage: "completed" } }
          )
            .then(() => {
              return Conversation.update(
                { _id: conversation._id },
                {
                  $set: {
                    stage: "completed",
                  },
                }
              );
            })
            .then(() => {
              videoTranscribed({
                videoId: updatedTranscription.videoId,
                slides: updatedTranscription.slides,
                status: "done",
              });
              resolve({
                textMessages: [
                  "Thank you, the video is transcibed successfully\n\nType *Start* to start a new conversation",
                ],
                to: msg.sender,
              });
            })
            .catch(reject);
        } else {
          cutVideo(
            updatedTranscription.videoUrl,
            updatedTranscription.slides[
              updatedTranscription.currentSlideNumber - 1
            ].startTime,
            updatedTranscription.slides[
              updatedTranscription.currentSlideNumber - 1
            ].endTime -
              updatedTranscription.slides[
                updatedTranscription.currentSlideNumber - 1
              ].startTime
          )
            .then((videoPath) => {
              resolve({
                videoPath,
                videoIndex: 0,
                textMessages: [
                  `Type the audio you hear in slide ${updatedTranscription.currentSlideNumber}/${updatedTranscription.slides.length}`,
                ],
                to: msg.sender,
              });
            })
            .catch(reject);
        }
      })
      .catch(reject);
  });
};

module.exports = handleTranscribingStage;
