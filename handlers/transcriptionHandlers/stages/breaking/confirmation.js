const Conversation = require("../../../../models/Conversation");
const BrokenVideo = require("../../../../models/BrokenVideo");
const { videoBroken } = require("../../../../events/emitters/platformEvents");
const { getSpeakersList } = require("../../../utils/helpers");
const { sendToUser } = require("../../../../events/emitters/botEvents");

const handleConfirmationStage = (msg, brokenVideo, conversation) => {
  return new Promise((resolve, reject) => {
    if (msg.content.toLowerCase() === "yes") {
      const updates = {};
      if (
        brokenVideo.duration <=
        brokenVideo.slides[brokenVideo.currentSlideNumber - 1].endTime
      ) {
        updates.currentSlideNumber = 0;
        updates.stage = "pending_transcribing";
      } else {
        updates.currentSlideNumber = brokenVideo.currentSlideNumber + 1;
        updates.stage = "breaking";
      }
      return BrokenVideo.update({ _id: brokenVideo._id }, { $set: updates })
        .then(() => {
          if (updates.stage === "pending_transcribing") {
            Conversation.update(
              { _id: conversation._id },
              {
                $set: {
                  stage: "completed",
                },
              }
            )
              .then(() => {
                videoBroken({
                  videoId: brokenVideo.videoId,
                  slides: brokenVideo.slides,
                  status: "done",
                });
                if (brokenVideo.requestedBy) {
                  sendToUser({
                    to: brokenVideo.requestedBy,
                    textMessages: [
                      `The video (*${brokenVideo.title}*) has been broken successfully and ready for stage 2\n\nTo start listening to audio and typing text reply with (*hi transcribevideo-${brokenVideo.videoId}*)`,
                    ],
                  });
                }
                return BrokenVideo.count({ stage: "pending_breaking" });
              })
              .then((pendingVideosCount) => {
                return resolve({
                  textMessages: [
                    `There ${
                      pendingVideosCount === 1 ? "is" : "are"
                    } ${pendingVideosCount} ${
                      pendingVideosCount === 1 ? "video" : "videos"
                    } available for breaking`,
                    "The video is broken successfully\n\nType *start* to start new conversation",
                  ],
                  to: msg.sender,
                });
              })
              .catch(reject);
          } else {
            return resolve({
              textMessages: [
                `When should slide ${
                  updates.currentSlideNumber
                } end?\n\nChoose who is speaking in the slide also:\n\nNo Speaker\n\n${getSpeakersList(
                  brokenVideo.numberOfSpeakers
                )}`,
              ],
              to: msg.sender,
            });
          }
        })
        .catch(reject);
    } else if (msg.content.toLowerCase() === "no") {
      return BrokenVideo.update(
        { _id: brokenVideo._id },
        {
          $set: {
            stage: "breaking",
          },
        }
      )
        .then(() => {
          return resolve({
            textMessages: [
              `When should slide ${
                brokenVideo.currentSlideNumber
              } end?\n\nChoose who is speaking in the slide also:\n\nNo Speaker\n\n${getSpeakersList(
                brokenVideo.numberOfSpeakers
              )}`,
            ],
            to: msg.sender,
          });
        })
        .catch(reject);
    }
    return resolve({
      textMessages: ["Invalid input"],
      to: msg.sender,
    });
  });
};

module.exports = handleConfirmationStage;
