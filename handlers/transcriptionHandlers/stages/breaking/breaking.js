const moment = require("moment");
const BrokenVideo = require("../../../../models/BrokenVideo");
const { cutVideo, getSpeakersRange } = require("../../../utils/helpers");

const handleBreakingStage = (msg, brokenVideo) => {
  return new Promise((resolve, reject) => {
    const data = msg.content.split(" ");
    const time = data[0];
    const timeArray = time.split(":");
    const minutes = timeArray[0];
    const seconds = timeArray[1];
    const speakerWord = data[1];
    const speakerNumber = data[2];
    if (
      data.length === 3 &&
      timeArray.length === 2 &&
      moment(time, "mm:ss", true).isValid()
    ) {
      let targetDuration =
        brokenVideo.currentSlideNumber === 1
          ? Number(seconds) + Number(minutes) * 60
          : Number(seconds) +
            Number(minutes) * 60 -
            brokenVideo.slides[brokenVideo.currentSlideNumber - 2].endTime;
      const prevEndTime =
        brokenVideo.currentSlideNumber === 1
          ? 0
          : brokenVideo.slides[brokenVideo.currentSlideNumber - 2].endTime;
      if (prevEndTime + targetDuration > brokenVideo.duration) {
        targetDuration = brokenVideo.duration - prevEndTime;
      }
      if (
        speakerWord.toLowerCase() === "no" &&
        speakerNumber.toLowerCase() === "speaker"
      ) {
        if (
          Number(minutes) <= 59 &&
          Number(seconds) <= 59 &&
          minutes.length === 2 &&
          seconds.length === 2 &&
          targetDuration > 0 &&
          targetDuration <= brokenVideo.duration - prevEndTime
        ) {
          return getResponse(msg, brokenVideo, prevEndTime, targetDuration, -1)
            .then(resolve)
            .catch(reject);
        }
        return resolve({
          textMessages: ["Please send a valid end time"],
          to: msg.sender,
        });
      } else if (
        speakerWord.toLowerCase() === "speaker" &&
        getSpeakersRange(brokenVideo.numberOfSpeakers).includes(
          Number(speakerNumber)
        )
      ) {
        if (
          Number(minutes) <= 59 &&
          Number(seconds) <= 59 &&
          minutes.length === 2 &&
          seconds.length === 2 &&
          targetDuration > 0 &&
          targetDuration <= 30 &&
          targetDuration <= brokenVideo.duration - prevEndTime
        ) {
          return getResponse(
            msg,
            brokenVideo,
            prevEndTime,
            targetDuration,
            Number(speakerNumber)
          )
            .then(resolve)
            .catch(reject);
        }
        return resolve({
          textMessages: ["Please send a valid end time"],
          to: msg.sender,
        });
      }
      return resolve({
        textMessages: ["Please send a valid speaker"],
        to: msg.sender,
      });
    }

    return resolve({
      textMessages: ["Please send valid data"],
      to: msg.sender,
    });
  });
};

const getResponse = (
  msg,
  brokenVideo,
  prevEndTime,
  targetDuration,
  speakerNumber
) => {
  return new Promise((resolve, reject) => {
    let cutPath;
    cutVideo(brokenVideo.videoUrl, prevEndTime, targetDuration)
      .then((cp) => {
        cutPath = cp;
        brokenVideo.slides[brokenVideo.currentSlideNumber - 1] = {
          startTime: prevEndTime,
          endTime: prevEndTime + targetDuration,
          speakerNumber: speakerNumber === -1 ? -1 : speakerNumber,
          silent: speakerNumber === -1,
        };
        return BrokenVideo.update(
          { _id: brokenVideo._id },
          {
            $set: {
              stage: "confirmation",
              slides: brokenVideo.slides,
            },
          }
        );
      })
      .then(() => {
        const response = resolve({
          videoPath: cutPath,
          videoIndex: 0,
          textMessages: [
            'Is this correct?\n\n👉 Send *"Yes"* to go to next slide.\n\n👉 Send *"No"* to try again.',
          ],
          to: msg.sender,
        });
        if (targetDuration > 15 && speakerNumber !== -1) {
          response.textMessages.push(
            "⚠️ Warning: An ideal slide length should be 10-15 seconds (1-2 sentences max)"
          );
        }
        return resolve(response);
      })
      .catch(reject);
  });
};

module.exports = handleBreakingStage;
