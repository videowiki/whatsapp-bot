const Conversation = require("../../../../models/Conversation");
const BrokenVideo = require("../../../../models/BrokenVideo");
const { getSpeakersList } = require("../../../utils/helpers");
const {
  conversationMessages,
  WHITELISTED_NUMBERS,
} = require("../../../utils/constants");

const handleBreakingSelect = (msg, conversation) => {
  return new Promise((resolve, reject) => {
    if (WHITELISTED_NUMBERS.includes(msg.sender)) {
      return BrokenVideo.findOne({ stage: "pending_breaking" })
        .then((brokenVideo) => {
          if (brokenVideo) {
            return BrokenVideo.update(
              { _id: brokenVideo._id },
              {
                $set: {
                  stage: "breaking",
                  currentSlideNumber: 1,
                },
              }
            )
              .then(() => {
                return Conversation.update(
                  { _id: conversation._id },
                  {
                    $set: {
                      brokenVideo: brokenVideo._id,
                      stage: "started",
                      type: "brokenVideo",
                    },
                  }
                );
              })
              .then(() => {
                return resolve({
                  videoUrl: brokenVideo.videoUrl,
                  videoIndex: 0,
                  textMessages: [
                    `💡 *Tip*: The video duration is ${brokenVideo.duration} seconds`,
                    `When should slide 1 end?\n\nChoose who is speaking in the slide also:\n\nNo Speaker\n\n${getSpeakersList(
                      brokenVideo.numberOfSpeakers
                    )}`,
                    "⚠️ *Warning*:\n\nAn ideal slide length should be 10-15 seconds (1-2 sentences max)\n\nThe maximum slide length is 30 seconds",
                  ],
                  to: msg.sender,
                });
              })
              .catch(reject);
          }
          return Conversation.update(
            { _id: conversation._id },
            {
              $set: {
                brokenVideo: null,
                type: null,
                stage: "option_select",
              },
            }
          )
            .then(() => {
              return resolve({
                textMessages: [
                  "There's no videos available for breaking, please try again later",
                  conversationMessages.mainMenu,
                ],
                to: msg.sender,
              });
            })
            .catch(reject);
        })
        .catch(reject);
    }
    return resolve({
      textMessages: ["Breaking videos is not available for you number."],
      to: msg.sender,
    });
  });
};

module.exports = handleBreakingSelect;
