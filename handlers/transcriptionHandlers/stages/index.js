const { ACTION_CODES, conversationMessages } = require("../../utils/constants");
const { isAction } = require("../../utils/helpers");
const resetBreaking = require("../operations/breaking/reset");
const removeBreakingForSubtitles = require("../operations/breaking/removeForSubtitles");
const removeBreakingForTranslation = require("../operations/breaking/removeForTranslation");
const removeBreakingForTranscription = require("../operations/breaking/removeForTranscription");
const resetTranscription = require("../operations/transcribing/reset");
const removeTranscriptionForTranslation = require("../operations/transcribing/removeForTranslation");
const removeTranscriptionForSubtitles = require("../operations/transcribing/removeForSubtitles");
const removeTranscriptionForBreaking = require("../operations/transcribing/removeForBreaking");
const removeBreakingForVerification = require("../operations/breaking/removeForVerification");
const removeTranscriptionForVerification = require("../operations/transcribing/removeForVerification");
const handleBreakingStage = require("./breaking/breaking");
const handleConfirmationStage = require("./breaking/confirmation");
const handleLangSelectStage = require("./transcribing/langSelect");
const handleTranscribingStage = require("./transcribing/transcribing");

const handleBreakingStages = (msg, brokenVideo, conversation) => {
  return new Promise((resolve, reject) => {
    if (msg.type !== "text") {
      return resolve({
        textMessages: ["Please use text messages"],
        to: msg.sender,
      });
    }
    if (isAction(msg.content, ACTION_CODES.SHOW_MENU)) {
      resolve({
        textMessages: [conversationMessages.mainMenu],
        to: msg.sender,
      });
    } else if (isAction(msg.content, ACTION_CODES.PROOFREAD_BREAK_VIDEOS)) {
      resetBreaking(msg, brokenVideo, conversation).then(resolve).catch(reject);
    } else if (isAction(msg.content, ACTION_CODES.PROOFREAD_TYPE_TEXT)) {
      removeBreakingForTranscription(msg, brokenVideo, conversation)
        .then(resolve)
        .catch(reject);
    } else if (isAction(msg.content, ACTION_CODES.ADD_SUBTITLES)) {
      removeBreakingForSubtitles(msg, brokenVideo, conversation, "Subtitles")
        .then(resolve)
        .catch(reject);
    } else if (isAction(msg.content, ACTION_CODES.SELECT_TEXT_TRANSLATION)) {
      removeBreakingForTranslation(msg, brokenVideo, conversation, "text")
        .then(resolve)
        .catch(reject);
    } else if (isAction(msg.content, ACTION_CODES.SELECT_AUDIO_TRANSLATION)) {
      removeBreakingForTranslation(msg, brokenVideo, conversation, "audio")
        .then(resolve)
        .catch(reject);
    } else if (
      isAction(msg.content, ACTION_CODES.SELECT_TEXT_AND_AUDIO_TRANSLATION)
    ) {
      removeBreakingForTranslation(msg, brokenVideo, conversation, "text_audio")
        .then(resolve)
        .catch(reject);
    } else if (
      isAction(msg.content, ACTION_CODES.VERIFY_TEXT) ||
      isAction(msg.content, ACTION_CODES.VERIFY_VOICE)
    ) {
      removeBreakingForVerification(msg, brokenVideo, conversation)
        .then(resolve)
        .catch(reject);
    } else {
      if (brokenVideo.stage === "breaking") {
        handleBreakingStage(msg, brokenVideo).then(resolve).catch(reject);
      } else if (brokenVideo.stage === "confirmation") {
        handleConfirmationStage(msg, brokenVideo, conversation)
          .then(resolve)
          .catch(reject);
      }
    }
  });
};

const handleTranscriptionStages = (msg, transcription, conversation) => {
  return new Promise((resolve, reject) => {
    if (msg.type !== "text") {
      return resolve({
        textMessages: ["Please use text messages"],
        to: msg.sender,
      });
    }
    if (isAction(msg.content, ACTION_CODES.SHOW_MENU)) {
      resolve({
        textMessages: [conversationMessages.mainMenu],
        to: msg.sender,
      });
    } else if (isAction(msg.content, ACTION_CODES.PROOFREAD_BREAK_VIDEOS)) {
      removeTranscriptionForBreaking(msg, transcription, conversation)
        .then(resolve)
        .catch(reject);
    } else if (isAction(msg.content, ACTION_CODES.PROOFREAD_TYPE_TEXT)) {
      resetTranscription(msg, transcription, conversation)
        .then(resolve)
        .catch(reject);
    } else if (isAction(msg.content, ACTION_CODES.ADD_SUBTITLES)) {
      removeTranscriptionForSubtitles(
        msg,
        transcription,
        conversation,
        "Subtitles"
      )
        .then(resolve)
        .catch(reject);
    } else if (isAction(msg.content, ACTION_CODES.SELECT_TEXT_TRANSLATION)) {
      removeTranscriptionForTranslation(
        msg,
        transcription,
        conversation,
        "text"
      )
        .then(resolve)
        .catch(reject);
    } else if (isAction(msg.content, ACTION_CODES.SELECT_AUDIO_TRANSLATION)) {
      removeTranscriptionForTranslation(
        msg,
        transcription,
        conversation,
        "audio"
      )
        .then(resolve)
        .catch(reject);
    } else if (
      isAction(msg.content, ACTION_CODES.SELECT_TEXT_AND_AUDIO_TRANSLATION)
    ) {
      removeTranscriptionForTranslation(
        msg,
        transcription,
        conversation,
        "text_audio"
      )
        .then(resolve)
        .catch(reject);
    } else if (
      isAction(msg.content, ACTION_CODES.VERIFY_TEXT) ||
      isAction(msg.content, ACTION_CODES.VERIFY_VOICE)
    ) {
      removeTranscriptionForVerification(msg, transcription, conversation)
        .then(resolve)
        .catch(reject);
    } else {
      if (transcription.stage === "lang_select") {
        handleLangSelectStage(msg, transcription, conversation)
          .then(resolve)
          .catch(reject);
      } else if (transcription.stage === "transcribing") {
        handleTranscribingStage(msg, transcription, conversation)
          .then(resolve)
          .catch(reject);
      }
    }
  });
};

module.exports = { handleBreakingStages, handleTranscriptionStages };
