const Subtitle = require("../../../models/Subtitle");

const resetSubtitle = (msg, subtitle) => {
  return new Promise((resolve, reject) => {
    Subtitle.update(
      { _id: subtitle._id },
      {
        $set: {
          videoUrl: null,
          articleId: null,
          stage: "lang_select",
          slides: [],
          currentSlideNumber: 0,
          langCode: null,
        },
      }
    )
      .then(() => {
        resolve({
          textMessages: ["Which language do you know?"],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

module.exports = resetSubtitle;
