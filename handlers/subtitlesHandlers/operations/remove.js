const Conversation = require("../../../models/Conversation");
const Subtitle = require("../../../models/Subtitle");
const handleBreakingSelect = require("../../transcriptionHandlers/stages/breaking/select");
const handleTranscriptionSelect = require("../../transcriptionHandlers/stages/transcribing/select");
const handleVerificationSelect = require("../../verificationHandlers/stages/select");
const handleTranslationSelect = require("../../translationHandlers/stages/select");

const removeSubtitle = (
  msg,
  subtitle,
  conversation,
  newModel,
  translationType
) => {
  return new Promise((resolve, reject) => {
    Subtitle.deleteOne({ _id: subtitle._id })
      .then(() => {
        return Conversation.update(
          { _id: conversation._id },
          {
            $set: {
              stage: "option_select",
              type: null,
              subtitle: null,
            },
          }
        );
      })
      .then(() => {
        if (newModel === "BrokenVideo") {
          return handleBreakingSelect(msg, conversation)
            .then(resolve)
            .catch(reject);
        }
        if (newModel === "Transcription") {
          return handleTranscriptionSelect(msg, conversation)
            .then(resolve)
            .catch(reject);
        }
        if (newModel === "Verification") {
          return handleVerificationSelect(msg, conversation)
            .then(resolve)
            .catch(reject);
        }
        if (newModel === "Translation") {
          return handleTranslationSelect(msg, conversation, translationType)
            .then(resolve)
            .catch(reject);
        }
      })
      .catch(reject);
  });
};

module.exports = removeSubtitle;
