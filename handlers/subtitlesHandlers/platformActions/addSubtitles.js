const translationExportService = require("@videowiki/services/translationExport")(
  process.env.TRANSLATION_EXPORT_SERVICE_API_ROOT
);
const articleService = require("@videowiki/services/article")(
  process.env.ARTICLE_SERVICE_API_ROOT
);
const Subtitle = require("../../../models/Subtitle");
const Conversation = require("../../../models/Conversation");
const {
  getSubtitleSlides,
  getValidSubtitleSlide,
  cutVideo,
} = require("../../utils/helpers");

const handlePlatformAddSubtitlesAction = (msg, articleId) => {
  return new Promise((resolve, reject) => {
    return translationExportService
      .findOne({
        article: articleId,
        status: "done",
        sort: { created_at: -1 },
      })
      .then((translationExport) => {
        if (translationExport) {
          return articleService
            .findById(articleId)
            .then((article) => {
              let subtitle;
              const slides = getSubtitleSlides(article);
              const slidePosition = getValidSubtitleSlide(slides, 0);
              if (slidePosition === -1) {
                return resolve({
                  textMessages: [
                    "There is no videos available for adding subtitles",
                  ],
                  to: msg.sender,
                });
              }
              return Subtitle.create({
                stage: "end_time",
                articleId: article._id,
                videoUrl: translationExport.videoUrl,
                langCode: article.langCode,
                currentSlideNumber: slidePosition,
                slides,
              })
                .then((s) => {
                  subtitle = s;
                  return Conversation.create({
                    stage: "started",
                    subtitle: subtitle._id,
                    type: "subtitle",
                    contactNumber: msg.sender,
                  }).then(() => {
                    return cutVideo(
                      subtitle.videoUrl,
                      subtitle.slides[subtitle.currentSlideNumber].startTime,
                      subtitle.slides[subtitle.currentSlideNumber].endTime
                    );
                  });
                })
                .then((cutPath) => {
                  return resolve({
                    videoUrl:
                      "https://tailoredvideowiki.s3-eu-west-1.amazonaws.com/static/tutorials_media/whatsapp_tutorials/stage5_compressed.mp4",
                    videoPath: cutPath,
                    videoIndex: 1,
                    textMessages: [
                      "Here's a quick tutorial for how to add subtitles on WhatsApp:",
                      `When does this sentence *"${
                        subtitle.slides[subtitle.currentSlideNumber].sentences[
                          subtitle.slides[subtitle.currentSlideNumber]
                            .currentSentenceNumber
                        ].text
                      }"* end in the above slide?`,
                      "Type in mm:ss format",
                      "Example: 00:05",
                    ],
                    to: msg.sender,
                  });
                });
            })
            .catch(reject);
        }
        return resolve({
          textMessages: ["The translation must be exported"],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

module.exports = handlePlatformAddSubtitlesAction;
