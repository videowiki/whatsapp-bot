const translationExportService = require("@videowiki/services/translationExport")(
  process.env.TRANSLATION_EXPORT_SERVICE_API_ROOT
);
const articleService = require("@videowiki/services/article")(
  process.env.ARTICLE_SERVICE_API_ROOT
);
const Subtitle = require("../../../models/Subtitle");
const {
  getLangCode,
  ucfirst,
  getSubtitleSlides,
  getValidSubtitleSlide,
  cutVideo,
} = require("../../utils/helpers");

const handleLangSelectStage = (msg, subtitle) => {
  return new Promise((resolve, reject) => {
    const langCode = getLangCode(msg.content);
    if (!langCode) {
      return resolve({
        textMessages: ["Please select a valid language"],
        to: msg.sender,
      });
    }
    // const articleLangCode = mapLangCodeToArticleLangCode(langCode);
    const articleQuery = {
      articleType: "translation",
      exported: true,
      langCode,
      $or: [{ archived: false }, { archived: { $exists: false } }],
    };
    let article;
    let updatedSubtitle;
    return articleService
      .count(articleQuery)
      .then((count) => {
        return articleService.find({
          ...articleQuery,
          skip: Math.floor(Math.random() * count),
          limit: 1,
        });
      })
      .then((articles) => {
        if (!articles || articles.length === 0) {
          return resolve({
            textMessages: [
              `There is no articles available in ${ucfirst(
                msg.content
              )} language`,
            ],
            to: msg.sender,
          });
        }
        article = articles[0];
        return translationExportService.findOne({
          article: article._id,
          status: "done",
          sort: { created_at: -1 },
        });
      })
      .then((translationExport) => {
        if (translationExport) {
          const slides = getSubtitleSlides(article);
          const slidePosition = getValidSubtitleSlide(slides, 0);
          if (slidePosition === -1) {
            return resolve({
              textMessages: [
                "There is no videos available for adding subtitles",
              ],
              to: msg.sender,
            });
          }
          return Subtitle.findOneAndUpdate(
            { _id: subtitle._id },
            {
              $set: {
                stage: "end_time",
                articleId: article._id,
                videoUrl: translationExport.videoUrl,
                langCode: langCode,
                currentSlideNumber: slidePosition,
                slides,
              },
            },
            { new: true }
          );
        }
        return resolve({
          textMessages: ["The translation must be exported"],
          to: msg.sender,
        });
      })
      .then((us) => {
        updatedSubtitle = us;
        return cutVideo(
          updatedSubtitle.videoUrl,
          updatedSubtitle.slides[updatedSubtitle.currentSlideNumber].startTime,
          updatedSubtitle.slides[updatedSubtitle.currentSlideNumber].endTime -
            updatedSubtitle.slides[updatedSubtitle.currentSlideNumber].startTime
        );
      })
      .then((cutPath) => {
        return resolve({
          videoPath: cutPath,
          videoIndex: 0,
          textMessages: [
            `When does this sentence *"${
              updatedSubtitle.slides[updatedSubtitle.currentSlideNumber]
                .sentences[
                updatedSubtitle.slides[updatedSubtitle.currentSlideNumber]
                  .currentSentenceNumber
              ].text
            }"* end in the above slide?`,
            "Type in mm:ss format",
            "Example: 00:05",
          ],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

module.exports = handleLangSelectStage;
