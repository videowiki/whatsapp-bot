const Conversation = require("../../../models/Conversation");
const Subtitle = require("../../../models/Subtitle");

const handleSubtitlesSelect = (msg, conversation) => {
  return new Promise((resolve, reject) => {
    return Subtitle.create({
      stage: "lang_select",
    })
      .then((subtitle) => {
        return Conversation.updateOne(
          { _id: conversation._id },
          {
            $set: {
              stage: "started",
              subtitle: subtitle._id,
              type: "subtitle",
            },
          }
        );
      })
      .then(() => {
        return resolve({
          textMessages: ["Which language do you know?"],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

module.exports = handleSubtitlesSelect;
