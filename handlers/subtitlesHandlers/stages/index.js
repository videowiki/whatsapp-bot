const handleLangSelectStage = require("./langSelect");
const handleEndTimeStage = require("./endTime");
const removeSubtitle = require("../operations/remove");
const resetSubtitle = require("../operations/reset");
const { isAction } = require("../../utils/helpers");
const { ACTION_CODES } = require("../../utils/constants");

const handleSubtitleStages = (msg, subtitle, conversation) => {
  return new Promise((resolve, reject) => {
    if (msg.type !== "text") {
      return resolve({
        textMessages: ["Please use text messages"],
        to: msg.sender,
      });
    }
    if (isAction(msg.content, ACTION_CODES.PROOFREAD_BREAK_VIDEOS)) {
      return removeSubtitle(msg, subtitle, conversation, "BrokenVideo")
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.PROOFREAD_TYPE_TEXT)) {
      return removeSubtitle(msg, subtitle, conversation, "Transcription")
        .then(resolve)
        .catch(reject);
    }
    if (
      isAction(msg.content, ACTION_CODES.VERIFY_TEXT) ||
      isAction(msg.content, ACTION_CODES.VERIFY_TEXT)
    ) {
      return removeSubtitle(msg, subtitle, conversation, "Verification")
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.SELECT_TEXT_TRANSLATION)) {
      return removeSubtitle(msg, subtitle, conversation, "Translation", "text")
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.SELECT_AUDIO_TRANSLATION)) {
      return removeSubtitle(msg, subtitle, conversation, "Translation", "audio")
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.SELECT_TEXT_AND_AUDIO_TRANSLATION)) {
      return removeSubtitle(
        msg,
        subtitle,
        conversation,
        "Translation",
        "text_audio"
      )
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.ADD_SUBTITLES)) {
      return resetSubtitle(msg, subtitle).then(resolve).catch(reject);
    }
    if (subtitle.stage === "lang_select") {
      return handleLangSelectStage(msg, subtitle, conversation)
        .then(resolve)
        .catch(reject);
    }
    if (subtitle.stage === "end_time") {
      return handleEndTimeStage(msg, subtitle, conversation)
        .then(resolve)
        .catch(reject);
    }
  });
};

module.exports = handleSubtitleStages;
