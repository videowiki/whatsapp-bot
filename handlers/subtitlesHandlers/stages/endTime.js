const moment = require("moment");
const Conversation = require("../../../models/Conversation");
const Subtitle = require("../../../models/Subtitle");
const { getValidSubtitleSlide, cutVideo } = require("../../utils/helpers");

const handleEndTimeStage = (msg, subtitle, conversation) => {
  return new Promise((resolve, reject) => {
    if (!moment(msg.content, "mm:ss", true).isValid()) {
      return resolve({
        textMessages: ["Please send the end time in mm:ss format"],
        to: msg.sender,
      });
    }
    const time = msg.content.split(":");
    const prevSlideEndTime =
      subtitle.currentSlideNumber === 0
        ? 0
        : subtitle.slides[subtitle.currentSlideNumber - 1].endTime;
    const prevSentenceEndTime =
      subtitle.slides[subtitle.currentSlideNumber].currentSentenceNumber === 0
        ? subtitle.slides[subtitle.currentSlideNumber].startTime
        : subtitle.slides[subtitle.currentSlideNumber].sentences[
            subtitle.slides[subtitle.currentSlideNumber].currentSentenceNumber -
              1
          ].endTime;
    const endTime = Number(time[0]) * 60 + Number(time[1]) + prevSlideEndTime;
    if (
      endTime >= subtitle.slides[subtitle.currentSlideNumber].endTime ||
      endTime <= prevSentenceEndTime
    ) {
      return resolve({
        textMessages: ["Please send a valid end time"],
        to: msg.sender,
      });
    }
    if (subtitle.currentSlideNumber === subtitle.slides.length - 1) {
      return handleLastSlide(msg, subtitle, conversation, endTime)
        .then(resolve)
        .catch(reject);
    }
    return handleNotLastSlide(msg, subtitle, conversation, endTime)
      .then(resolve)
      .catch(reject);
  });
};

const handleNotLastSlide = (msg, subtitle, conversation, endTime) => {
  return new Promise((resolve, reject) => {
    const updates = {};
    if (
      subtitle.slides[subtitle.currentSlideNumber].currentSentenceNumber ===
      subtitle.slides[subtitle.currentSlideNumber].sentences.length - 2
    ) {
      // before last sentence
      subtitle.slides[subtitle.currentSlideNumber].sentences[
        subtitle.slides[subtitle.currentSlideNumber].currentSentenceNumber
      ].endTime = endTime;
      subtitle.slides[subtitle.currentSlideNumber].sentences[
        subtitle.slides[subtitle.currentSlideNumber].currentSentenceNumber + 1
      ].endTime = subtitle.slides[subtitle.currentSlideNumber].endTime;
      updates.slides = subtitle.slides;
      const slidePosition = getValidSubtitleSlide(
        subtitle.slides,
        subtitle.currentSlideNumber + 1
      );
      updates.stage = slidePosition ? "end_time" : "completed";
      updates.currentSlideNumber = slidePosition ? slidePosition : 0;
      let updatedSubtitle;
      return Subtitle.findOneAndUpdate(
        { _id: subtitle._id },
        {
          $set: updates,
        },
        { new: true }
      )
        .then((us) => {
          if (slidePosition === -1) {
            return Conversation.updateOne(
              { _id: conversation._id },
              { $set: { stage: "completed" } }
            ).then(() => {
              return resolve({
                textMessages: ["The subtitles are added successfully"],
                to: msg.sender,
              });
            });
          }
          updatedSubtitle = us;
          return cutVideo(
            updatedSubtitle.videoUrl,
            updatedSubtitle.slides[updatedSubtitle.currentSlideNumber]
              .startTime,
            updatedSubtitle.slides[updatedSubtitle.currentSlideNumber].endTime -
              updatedSubtitle.slides[updatedSubtitle.currentSlideNumber]
                .startTime
          );
        })
        .then((cutPath) => {
          return resolve({
            videoPath: cutPath,
            videoIndex: 0,
            textMessages: [
              `When does this sentence *"${
                updatedSubtitle.slides[updatedSubtitle.currentSlideNumber]
                  .sentences[
                  updatedSubtitle.slides[updatedSubtitle.currentSlideNumber]
                    .currentSentenceNumber
                ].text
              }"* end in the above slide?`,
              "Type in mm:ss format",
              "Example: 00:05",
            ],
            to: msg.sender,
          });
        })
        .catch(reject);
    }
    // not before last sentence
    subtitle.slides[subtitle.currentSlideNumber].sentences[
      subtitle.slides[subtitle.currentSlideNumber].currentSentenceNumber
    ].endTime = endTime;
    subtitle.slides[subtitle.currentSlideNumber].currentSentenceNumber =
      subtitle.slides[subtitle.currentSlideNumber].currentSentenceNumber + 1;
    updates.slides = subtitle.slides;
    return Subtitle.findOneAndUpdate(
      { _id: subtitle._id },
      {
        $set: updates,
      },
      { new: true }
    )
      .then((updatedSubtitle) => {
        return resolve({
          textMessages: [
            `When does this sentence *"${
              updatedSubtitle.slides[updatedSubtitle.currentSlideNumber]
                .sentences[
                updatedSubtitle.slides[updatedSubtitle.currentSlideNumber]
                  .currentSentenceNumber
              ].text
            }"* end in the above slide?`,
            "Type in mm:ss format",
            "Example: 00:05",
          ],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

const handleLastSlide = (msg, subtitle, conversation, endTime) => {
  return new Promise((resolve, reject) => {
    const updates = {};
    if (
      subtitle.slides[subtitle.currentSlideNumber].currentSentenceNumber ===
      subtitle.slides[subtitle.currentSlideNumber].sentences.length - 2
    ) {
      // before last sentence
      subtitle.slides[subtitle.currentSlideNumber].sentences[
        subtitle.slides[subtitle.currentSlideNumber].currentSentenceNumber
      ].endTime = endTime;
      subtitle.slides[subtitle.currentSlideNumber].sentences[
        subtitle.slides[subtitle.currentSlideNumber].currentSentenceNumber + 1
      ].endTime = subtitle.slides[subtitle.currentSlideNumber].endTime;
      updates.slides = subtitle.slides;
      updates.stage = "completed";
      updates.currentSlideNumber = 0;
      return Subtitle.findOneAndUpdate(
        { _id: subtitle._id },
        {
          $set: updates,
        }
      )
        .then(() => {
          return Conversation.updateOne(
            { _id: conversation._id },
            { $set: { stage: "completed" } }
          );
        })
        .then(() => {
          return resolve({
            textMessages: ["The subtitles are added successfully"],
            to: msg.sender,
          });
        })
        .catch(reject);
    }
    // not before last sentence
    subtitle.slides[subtitle.currentSlideNumber].sentences[
      subtitle.slides[subtitle.currentSlideNumber].currentSentenceNumber
    ].endTime = endTime;
    subtitle.slides[subtitle.currentSlideNumber].currentSentenceNumber =
      subtitle.slides[subtitle.currentSlideNumber].currentSentenceNumber + 1;
    updates.slides = subtitle.slides;
    return Subtitle.findOneAndUpdate(
      { _id: subtitle._id },
      {
        $set: updates,
      },
      { new: true }
    )
      .then((updatedSubtitle) => {
        return resolve({
          textMessages: [
            `When does this sentence *"${
              updatedSubtitle.slides[updatedSubtitle.currentSlideNumber]
                .sentences[
                updatedSubtitle.slides[updatedSubtitle.currentSlideNumber]
                  .currentSentenceNumber
              ].text
            }"* end in the above slide?`,
            "Type in mm:ss format",
            "Example: 00:05",
          ],
          to: msg.sender,
        });
      })
      .catch(reject);
  });
};

module.exports = handleEndTimeStage;
