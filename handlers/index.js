const Conversation = require("../models/Conversation");
const BrokenVideo = require("../models/BrokenVideo");
const {
  conversationMessages,
  ACTION_CODES,
  PLATFORM_ACTIONS,
  WHITELISTED_NUMBERS,
} = require("./utils/constants");
const { isAction, iSPlatformAction } = require("./utils/helpers");
const startNewConversation = require("./conversationHandlers/operations/start");
const resetConversation = require("./conversationHandlers/operations/reset");
const handleConversationStages = require("./conversationHandlers/stages");
const handlePlatformBreakingAction = require("./transcriptionHandlers/platformActions/breaking");
const handlePlatformTranscribingAction = require("./transcriptionHandlers/platformActions/transcribing");
const handlePlatformTranslationAction = require("./translationHandlers/platformActions/translation");
const handlePlatformVerificationAction = require("./verificationHandlers/platformActions/verify");
const handlePlatformAddSubtitlesAction = require("./subtitlesHandlers/platformActions/addSubtitles");
const handlePlatformNotifyOnProofreadingReadyAction = require("./transcriptionHandlers/platformActions/notifyOnProofreadingReady");
const removeConversation = require("./conversationHandlers/operations/remove");

const handleMsg = (msg) => {
  return new Promise((resolve, reject) => {
    if (iSPlatformAction(msg.content, PLATFORM_ACTIONS.BREAK_VIDEO)) {
      return handlePlatformBreakingAction(msg, msg.content.split("-")[1])
        .then(resolve)
        .catch(reject);
    }
    if (iSPlatformAction(msg.content, PLATFORM_ACTIONS.TRANSCRIBE_VIDEO)) {
      return removeConversation(msg)
        .then(() => {
          return handlePlatformTranscribingAction(
            msg,
            msg.content.split("-")[1]
          );
        })
        .then(resolve)
        .catch(reject);
    }
    if (iSPlatformAction(msg.content, PLATFORM_ACTIONS.TRANSLATE_VIDEO)) {
      return removeConversation(msg)
        .then(() => {
          return handlePlatformTranslationAction(
            msg,
            msg.content.split("-")[1],
            msg.content.split("-")[2]
          );
        })
        .then(resolve)
        .catch(reject);
    }
    if (
      iSPlatformAction(msg.content, PLATFORM_ACTIONS.VERIFY_TEXT_TRANSLATION)
    ) {
      return removeConversation(msg)
        .then(() => {
          return handlePlatformVerificationAction(
            msg,
            msg.content.split("-")[2],
            "text"
          );
        })
        .then(resolve)
        .catch(reject);
    }
    if (
      iSPlatformAction(msg.content, PLATFORM_ACTIONS.VERIFY_VOICE_TRANSLATION)
    ) {
      return removeConversation(msg)
        .then(() => {
          return handlePlatformVerificationAction(
            msg,
            msg.content.split("-")[2],
            "voice"
          );
        })
        .then(resolve)
        .catch(reject);
    }
    if (iSPlatformAction(msg.content, PLATFORM_ACTIONS.ADD_SUBTITLES)) {
      return removeConversation(msg)
        .then(() => {
          return handlePlatformAddSubtitlesAction(
            msg,
            msg.content.split("-")[1]
          );
        })
        .then(resolve)
        .catch(reject);
    }
    if (
      iSPlatformAction(
        msg.content,
        PLATFORM_ACTIONS.NOTIFY_ON_PROOFREADING_READY
      )
    ) {
      return handlePlatformNotifyOnProofreadingReadyAction(
        msg,
        msg.content.split("-")[1]
      )
        .then(resolve)
        .catch(reject);
    }
    if (
      msg.content.toLowerCase() === "pending" &&
      WHITELISTED_NUMBERS.includes(msg.sender)
    ) {
      return BrokenVideo.count({ stage: "pending_breaking" })
        .then((pendingVideosCount) => {
          return resolve({
            textMessages: [
              `There ${
                pendingVideosCount === 1 ? "is" : "are"
              } ${pendingVideosCount} ${
                pendingVideosCount === 1 ? "video" : "videos"
              } available for breaking`,
            ],
            to: msg.sender,
          });
        })
        .catch(reject);
    }
    Conversation.find({ contactNumber: msg.sender }).then((conversations) => {
      if (conversations.length) {
        return Conversation.findOne({
          contactNumber: msg.sender,
          stage: { $ne: "completed" },
        })
          .then((conversation) => {
            if (conversation) {
              if (isAction(msg.content, ACTION_CODES.RESTART)) {
                if (conversation.stage === "option_select") {
                  return resolve({
                    textMessages: [conversationMessages.mainMenu],
                    to: msg.sender,
                  });
                }
                if (conversation.stage === "started") {
                  return resetConversation(msg, conversation)
                    .then(resolve)
                    .catch(reject);
                }
              }
              return handleConversationStages(conversation, msg)
                .then(resolve)
                .catch(reject);
            }
            if (isAction(msg.content, ACTION_CODES.SHOW_MENU)) {
              return resolve({
                textMessages: [conversationMessages.mainMenu],
                to: msg.sender,
              });
            }
            if (isAction(msg.content, ACTION_CODES.START)) {
              return startNewConversation(msg).then(resolve).catch(reject);
            }
            return resolve({
              textMessages: [conversationMessages.invalidInput],
              to: msg.sender,
            });
          })
          .catch(reject);
      }
      return startNewConversation(msg).then(resolve).catch(reject);
    });
  });
};

module.exports = handleMsg;
