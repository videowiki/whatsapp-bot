const {
  ARTICLE_LANGCODE_MAP,
  isoLangs,
  GOOGLE_CREDENTIALS,
} = require("./constants");

const {
  googleProjectId,
  googleClientEmail,
  googlePrivateKey,
} = GOOGLE_CREDENTIALS;

const { Translate } = require("@google-cloud/translate").v2;

const translate = new Translate({
  projectId: googleProjectId,
  credentials: {
    client_email: googleClientEmail,
    private_key: googlePrivateKey,
  },
});

const request = require("superagent");
const path = require("path");
const fs = require("fs");
const uuid = require("uuid").v4;
const musicMetadata = require("music-metadata");
const { exec } = require("child_process");

const isAction = (text, action) => {
  return action.some((a) => a === text.toLowerCase());
};

const iSPlatformAction = (text, action) => {
  return action.test(text.trim().toLowerCase());
};

const ucfirst = (string) => {
  return (
    string.toLowerCase().charAt(0).toUpperCase() + string.toLowerCase().slice(1)
  );
};

const ucfirstOnly = (string) => {
  return string.toLowerCase().charAt(0).toUpperCase() + string.slice(1);
};

const getLangCode = (lang) => {
  const langName = ucfirst(lang);

  for (let code in isoLangs) {
    if (langName == isoLangs[code].name) {
      return code;
    }
  }
};

const mapLangCodeToArticleLangCode = (langCode) => {
  if (ARTICLE_LANGCODE_MAP[langCode]) {
    return ARTICLE_LANGCODE_MAP[langCode];
  }
  return langCode;
};

const getLangInEnglish = (langName) => {
  return new Promise((resolve, reject) => {
    translate
      .translate(langName, "en")
      .then((translation) => {
        resolve(translation[0]);
      })
      .catch(reject);
  });
};

const translateText = (text, langCode) => {
  return new Promise((resolve, reject) => {
    translate
      .translate(text, langCode)
      .then((translation) => {
        // console.log(translation[1].data.translations[0])

        resolve(translation[0]);
      })
      .catch(reject);
  });
};

const detectLang = (text) => {
  return new Promise((resolve, reject) => {
    translate
      .detect(text)
      .then((response) => {
        resolve(response[0].language);
      })
      .catch(reject);
  });
};

const getFileDurationFromBuffer = (buffer) => {
  return new Promise((resolve, reject) => {
    fs.writeFile("voice.ogg", buffer, (err) => {
      if (err) {
        fs.unlink("voice.ogg", (err) => {
          console.log(err);
        });
        reject(err);
      } else {
        const cmd =
          'ffprobe -i voice.ogg -show_entries format=duration -v quiet -of csv="p=0"';
        exec(cmd, (error, stdout, stderr) => {
          fs.unlink("voice.ogg", (err) => {
            if (err) console.log(err);
          });
          if (error) console.error(`exec error: ${error}`);
          if (stderr) console.error(`stderr: ${stderr}`);
          let response = JSON.parse(stdout);
          if (response) {
            resolve(response);
          }
          reject(error);
        });
      }
    });
  });
};

const getFileDuration = (url) => {
  return new Promise((resolve, reject) => {
    const filePath = url.split("/").pop();

    downloadFile(url, filePath)
      .then(() => {
        return musicMetadata.parseFile(filePath);
      })
      .then((md) => {
        fs.unlink(filePath, (e) => {
          if (e) console.log("removed file", e);
        });
        return resolve(md.format.duration);
      })
      .catch(reject);
  });
};

const downloadFile = (url, targetPath) => {
  return new Promise((resolve, reject) => {
    const extension = url.split(".").pop();
    const filePath =
      targetPath || path.join(__dirname, `${uuid()}.${extension}`);
    const stream = fs.createWriteStream(filePath);
    stream.on("finish", () => resolve(filePath));
    stream.on("error", (err) => reject(err));
    // Start download
    request.get(url).pipe(stream);
  });
};

const cutVideo = (videoUrl, start, duration) => {
  return new Promise((resolve, reject) => {
    downloadFile(videoUrl)
      .then((videoPath) => {
        // const duration = end - start
        const extension = videoUrl.split(".").pop();
        const cutPath = path.join(__dirname, `${uuid()}.${extension}`);
        const cmd = `ffmpeg -i ${videoPath} -ss ${start} -t ${duration} ${cutPath}`;
        console.log("cutting video");
        console.log(cmd);
        exec(cmd, (err) => {
          fs.unlink(videoPath, (e) => {
            if (e) console.log("removed file", e);
          });
          if (err) {
            console.log(err);
            return reject(err);
          }
          resolve(cutPath);
        });
      })
      .catch(reject);
  });
};

const compressVideo = (url) => {
  return new Promise((resolve, reject) => {
    downloadFile(url)
      .then((videoPath) => {
        const extension = url.split(".").pop();
        const compressPath = path.join(__dirname, `${uuid()}.${extension}`);
        const cmd = `ffmpeg -i ${videoPath} -filter:v scale=380:-1 -c:a copy ${compressPath}`;
        exec(cmd, () => {
          fs.unlink(videoPath, (e) => {
            if (e) console.log("removed file", e);
          });
          resolve(compressPath);
        });
      })
      .catch(reject);
  });
};

const convertAudioToMP3 = (url) => {
  return new Promise((resolve, reject) => {
    downloadFile(url)
      .then((audioPath) => {
        const convertedPath = path.join(__dirname, `${uuid()}.mp3`);
        const cmd = `ffmpeg -i ${audioPath} ${convertedPath}`;
        exec(cmd, () => {
          fs.unlink(audioPath, (e) => {
            if (e) console.log("removed file", e);
          });
          resolve(convertedPath);
        });
      })
      .catch(reject);
  });
};

const getSpeakersList = (count) => {
  let list = "";
  for (let i = 1; i <= count; i++) {
    list += `Speaker ${i}\n\n`;
  }
  return list;
};

const getSpeakersRange = (numberOfSpeakers) => {
  const speakersRange = [];
  for (let i = 1; i <= numberOfSpeakers; i++) {
    speakersRange.push(i);
  }
  return speakersRange;
};

const getNotSilentSlidePosition = (slides, start) => {
  for (let i = start; i < slides.length; i++) {
    if (slides[i].silent === false) {
      return i;
    }
  }
  return -1;
};

const getTranscriptionSlides = (article) => {
  let slides = [];
  article.slides.forEach((slide) => {
    slide.content.forEach((subslide) => {
      slides.push({
        startTime: subslide.startTime,
        endTime: subslide.endTime,
        silent: subslide.speakerProfile.speakerNumber === -1,
        slidePosition: slide.position,
        subslidePosition: subslide.position,
        speakerNumber: subslide.speakerProfile.speakerNumber,
      });
    });
  });
  return slides;
};

const getTranslationSlides = (article) => {
  let originalSlides = [];
  article.slides.forEach((slide) => {
    slide.content.forEach((subslide) => {
      if (subslide.speakerProfile.speakerNumber !== -1) {
        originalSlides.push({
          text: subslide.text,
          videoUrl: subslide.media[0].url,
          duration: subslide.media[0].duration,
          audio: subslide.audio,
          slidePosition: slide.position,
          subslidePosition: subslide.position,
        });
      }
    });
  });
  let translatedSlides = JSON.parse(JSON.stringify(originalSlides));
  translatedSlides.forEach((slide) => {
    delete slide.text;
    delete slide.audio;
  });
  return { originalSlides, translatedSlides };
};

const getVerificationSlides = (originalArticle, translatedArticle) => {
  let slides = [];
  let originalArticleSlides = getTranslationSlides(originalArticle)
    .originalSlides;
  let translatedArticleSlides = getTranslationSlides(translatedArticle)
    .originalSlides;
  let originalTexts = originalArticleSlides.map((oas) => {
    return oas.text;
  });
  translatedArticleSlides.forEach((tas, i) => {
    slides[i] = {
      originalText: originalTexts[i],
      translatedText: tas.text,
      videoUrl: tas.videoUrl,
      translatedAudioUrl: tas.audio,
      slidePosition: tas.slidePosition,
      subslidePosition: tas.subslidePosition,
    };
  });
  return slides;
};

const getSubtitleSlides = (article) => {
  let slides = [];
  article.slides.forEach((slide) => {
    slide.content.forEach((subslide) => {
      // if (subslide.speakerProfile.speakerNumber !== -1) {
      slides.push({
        text: subslide.text,
        slidePosition: slide.position,
        subslidePosition: subslide.position,
        startTime: subslide.startTime,
        endTime: subslide.endTime,
        silent: subslide.speakerProfile.speakerNumber === -1,
      });
      // }
    });
  });
  slides.forEach((slide) => {
    const sentences = [];
    const texts = slide.text
      .trim()
      .split(getSubtitleDelimiter(article.langCode))
      .filter((text) => text);
    texts.forEach((sentenceText, i) => {
      sentences.push({ text: sentenceText });
      if (texts.length === 1) {
        sentences[i].endTime = slide.endTime;
      }
      slide.sentences = sentences;
    });
  });
  return slides;
};

const getValidSubtitleSlide = (slides, start) => {
  for (let i = start; i < slides.length; i++) {
    if (slides[i].sentences.length > 1 && slides[i].silent === false) {
      return i;
    }
  }
  return -1;
};

const getSubtitleDelimiter = (langCode) => {
  return langCode === "hi" ? "।" : ".";
};

module.exports = {
  isAction,
  iSPlatformAction,
  ucfirst,
  ucfirstOnly,
  getLangCode,
  getLangInEnglish,
  getFileDurationFromBuffer,
  getFileDuration,
  cutVideo,
  compressVideo,
  convertAudioToMP3,
  detectLang,
  translateText,
  getSpeakersRange,
  getSpeakersList,
  getNotSilentSlidePosition,
  getTranscriptionSlides,
  getTranslationSlides,
  getVerificationSlides,
  getSubtitleSlides,
  getSubtitleDelimiter,
  getValidSubtitleSlide,
  mapLangCodeToArticleLangCode,
};
