const fs = require("fs");
const request = require("superagent");
const storageService = require("@videowiki/services/storage")(
  process.env.STORAGE_SERVICE_API_ROOT
);
const API_GATEWAY_SERVICE_API_ROOT = process.env.API_GATEWAY_SERVICE_API_ROOT;
const { compressVideo, getFileDuration } = require("../utils/helpers");

const fetchVideoById = (id) => {
  return new Promise((resolve, reject) => {
    request
      .get(`${API_GATEWAY_SERVICE_API_ROOT}/api/video/${id}`)
      .set("Content-Type", "application/json")
      .then((response) => {
        const video = response.body;
        let compressPath;
        if (video) {
          return compressVideo(video.url)
            .then((cp) => {
              compressPath = cp;
              return storageService.saveFile(
                "whatsapp-bot/videos",
                compressPath.split("/").pop(),
                fs.createReadStream(compressPath)
              );
            })
            .then(({ url }) => {
              fs.unlink(compressPath, (err) => {
                if (err) console.log("removed file", err);
              });
              video.url = url;
              return getFileDuration(video.url);
            })
            .then((duration) => {
              video.duration = duration;
              return resolve(video);
            })
            .catch(reject);
        }
        return resolve();
      })
      .catch(resolve);
  });
};

const fetchArticleById = (id) => {
  return new Promise((resolve, reject) => {
    request
      .get(`${API_GATEWAY_SERVICE_API_ROOT}/api/article/${id}`)
      .set("Content-Type", "application/json")
      .then((response) => {
        // console.log(response);
        const article = response.body.article;
        if (article) {
          resolve(article);
        } else {
          resolve(null);
        }
      })
      .catch(resolve);
  });
};

const fetchTranslationArticle = (langFrom) => {
  return new Promise((resolve, reject) => {
    request
      .get(
        `${API_GATEWAY_SERVICE_API_ROOT}/api/article/whatsapp-bot?langFrom=${langFrom}`
      )
      .set("Content-Type", "application/json")
      .then(({ body: { article } }) => {
        resolve(article);
      })
      .catch(reject);
  });
};

module.exports = { fetchVideoById, fetchArticleById, fetchTranslationArticle };
