const handleOptionSelectStage = require("./optionSelect");
const handleStartedStage = require("./started");

const handleConversationStages = (conversation, msg) => {
  return new Promise((resolve, reject) => {
    if (conversation.stage === "option_select") {
      return handleOptionSelectStage(msg, conversation)
        .then(resolve)
        .catch(reject);
    }
    if (conversation.stage === "started") {
      return handleStartedStage(msg, conversation).then(resolve).catch(reject);
    }
  });
};

module.exports = handleConversationStages;
