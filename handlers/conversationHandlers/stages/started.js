const { ucfirstOnly } = require("../../utils/helpers");
const Translation = require("../../../models/Translation");
const Transcription = require("../../../models/Transcription");
const BrokenVideo = require("../../../models/BrokenVideo");
const Verification = require("../../../models/Verification");
const Subtitle = require("../../../models/Subtitle");
const {
  handleBreakingStages,
  handleTranscriptionStages,
} = require("../../transcriptionHandlers/stages");
const handleTranslationStages = require("../../translationHandlers/stages");
const handleVerificationStages = require("../../verificationHandlers/stages");
const handleSubtitleStages = require("../../subtitlesHandlers/stages/index");

const handleStartedStage = (msg, conversation) => {
  return new Promise((resolve, reject) => {
    const model = ucfirstOnly(conversation.type);
    if (model === "Translation") {
      return Translation.findById(conversation[conversation.type])
        .then((translation) => {
          handleTranslationStages(msg, translation, conversation)
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    }
    if (model === "BrokenVideo") {
      return BrokenVideo.findById(conversation[conversation.type])
        .then((BrokenVideo) => {
          handleBreakingStages(msg, BrokenVideo, conversation)
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    }
    if (model === "Transcription") {
      return Transcription.findById(conversation[conversation.type])
        .then((transcription) => {
          handleTranscriptionStages(msg, transcription, conversation)
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    }
    if (model === "Verification") {
      return Verification.findById(conversation[conversation.type])
        .then((verification) => {
          handleVerificationStages(msg, verification, conversation)
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    }
    if (model === "Subtitle") {
      return Subtitle.findById(conversation[conversation.type])
        .then((subtitle) => {
          handleSubtitleStages(msg, subtitle, conversation)
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    }
  });
};

module.exports = handleStartedStage;
