const { conversationMessages, ACTION_CODES } = require("../../utils/constants");
const { isAction } = require("../../utils/helpers");
const handleTranscriptionSelect = require("../../transcriptionHandlers/stages/transcribing/select");
const handleBreakingSelect = require("../../transcriptionHandlers/stages/breaking/select");
const handleTranslationSelect = require("../../translationHandlers/stages/select");
const handleVerificationSelect = require("../../verificationHandlers/stages/select");
const handleSubtitlesSelect = require("../../subtitlesHandlers/stages/select");

const handleOptionSelectStage = (msg, conversation) => {
  return new Promise((resolve, reject) => {
    if (isAction(msg.content, ACTION_CODES.SHOW_MENU)) {
      return resolve({
        textMessages: [conversationMessages.mainMenu],
        to: msg.sender,
      });
    }
    if (isAction(msg.content, ACTION_CODES.PROOFREAD_BREAK_VIDEOS)) {
      return handleBreakingSelect(msg, conversation)
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.PROOFREAD_TYPE_TEXT)) {
      return handleTranscriptionSelect(msg, conversation)
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.SELECT_TEXT_TRANSLATION)) {
      return handleTranslationSelect(msg, conversation, "text")
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.SELECT_AUDIO_TRANSLATION)) {
      return handleTranslationSelect(msg, conversation, "audio")
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.SELECT_TEXT_AND_AUDIO_TRANSLATION)) {
      return handleTranslationSelect(msg, conversation, "text_audio")
        .then(resolve)
        .catch(reject);
    }
    if (isAction(msg.content, ACTION_CODES.ADD_SUBTITLES)) {
      return handleSubtitlesSelect(msg, conversation)
        .then(resolve)
        .catch(reject);
    }
    if (
      isAction(msg.content, ACTION_CODES.VERIFY_TEXT) ||
      isAction(msg.content, ACTION_CODES.VERIFY_VOICE)
    ) {
      return handleVerificationSelect(msg).then(resolve).catch(reject);
    }
    return resolve({
      textMessages: [conversationMessages.invalidInput],
      to: msg.sender,
    });
  });
};

module.exports = handleOptionSelectStage;
