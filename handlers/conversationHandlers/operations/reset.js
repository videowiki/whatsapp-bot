const Conversation = require("../../../models/Conversation");
const Translation = require("../../../models/Translation");
const BrokenVideo = require("../../../models/BrokenVideo");
const Transcription = require("../../../models/Transcription");
const Verification = require("../../../models/Verification");
const Subtitle = require("../../../models/Subtitle");
const { conversationMessages } = require("../../utils/constants");
const { ucfirst } = require("../../utils/helpers");

const resetConversation = (msg, conversation) => {
  return new Promise((resolve, reject) => {
    const typeId = conversation[conversation.type];
    const model = ucfirst(conversation.type);
    Conversation.update(
      { _id: conversation._id },
      {
        $set: {
          stage: "option_select",
          type: undefined,
          translation: undefined,
          brokenVideo: undefined,
          transcription: undefined,
        },
      }
    )
      .then(() => {
        if (model === "Translation") {
          return Translation.deleteOne({ _id: typeId })
            .then(() => {
              resolve({
                textMessages: [conversationMessages.mainMenu],
                to: msg.sender,
              });
            })
            .catch(reject);
        }
        if (model === "BrokenVideo") {
          BrokenVideo.update(
            { _id: typeId },
            {
              $set: {
                stage: "pending_breaking",
                currentSlideNumber: 0,
                slides: [],
              },
            }
          )
            .then(() => {
              resolve({
                textMessages: [conversationMessages.mainMenu],
                to: msg.sender,
              });
            })
            .catch(reject);
        }
        if (model === "Transcription") {
          return Transcription.deleteOne({ _id: typeId })
            .then(() => {
              resolve({
                textMessages: [conversationMessages.mainMenu],
                to: msg.sender,
              });
            })
            .catch(reject);
        }
        if (model === "Verification") {
          return Verification.deleteOne({ _id: typeId })
            .then(() => {
              resolve({
                textMessages: [conversationMessages.mainMenu],
                to: msg.sender,
              });
            })
            .catch(reject);
        }
        if (model === "Subtitle") {
          return Subtitle.deleteOne({ _id: typeId })
            .then(() => {
              resolve({
                textMessages: [conversationMessages.mainMenu],
                to: msg.sender,
              });
            })
            .catch(reject);
        }
      })
      .catch(reject);
  });
};

module.exports = resetConversation;
