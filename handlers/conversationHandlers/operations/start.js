const Conversation = require("../../../models/Conversation");
const { conversationMessages } = require("../../utils/constants");

const startNewConversation = (msg) => {
  return new Promise((resolve, reject) => {
    Conversation.create({
      stage: "option_select",
      contactNumber: msg.sender,
    })
      .then(
        resolve({
          textMessages: [conversationMessages.mainMenu],
          to: msg.sender,
        })
      )
      .catch(reject);
  });
};

module.exports = startNewConversation;
