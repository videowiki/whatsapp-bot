const Conversation = require("../../../models/Conversation");
const BrokenVideo = require("../../../models/BrokenVideo");
const { ucfirstOnly } = require("../../utils/helpers");

const removeConversation = (msg) => {
  return new Promise((resolve, reject) => {
    return Conversation.findOneAndDelete({
      contactNumber: msg.sender,
      stage: { $ne: "completed" },
    })
      .then((conversation) => {
        if (!conversation || !conversation.type) {
          return resolve();
        }
        if (conversation && conversation.type === "brokenVideo") {
          return BrokenVideo.update(
            { _id: conversation.brokenVideo },
            {
              $set: {
                stage: "pending_breaking",
                currentSlideNumber: 0,
                slides: [],
              },
            }
          )
            .then(resolve)
            .catch(reject);
        }
        if (conversation && conversation.type) {
          const model = require(`../../../models/${ucfirstOnly(
            conversation.type
          )}`);
          return model
            .deleteOne({
              _id: conversation[conversation.type],
            })
            .then(resolve)
            .catch(reject);
        }
      })
      .catch(reject);
  });
};

module.exports = removeConversation;
