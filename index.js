const express = require("express");
const app = express();
app.use(express.json());
const morgan = require("morgan");
app.use(morgan("dev"));
const dotenv = require("dotenv");
dotenv.config();
const CronJob = require('cron').CronJob

const mongoose = require("mongoose");
const { login } = require("./turn.io");
const DB_CONNECTION_URL = process.env.WHATSAPP_BOT_DATABASE_URL;
const username = process.env.TURNIO_USERNAME;
const password = process.env.TURNIO_PASSWORD;
const port = process.env.PORT || 4000;
const handleMsg = require("./handlers");
const { sendToUser } = require("./events/emitters/botEvents");
const createEventsChannel = require("./events/createChannel");

let mongoConnection;

mongoose
  .connect(DB_CONNECTION_URL)
  .then((con) => {
    console.log('database connected');
    mongoConnection = con.connection;
    con.connection.on('disconnected', () => {
        console.log('Database disconnected! shutting down service')
        process.exit(1);
    })
  })
  .catch((err) => {
    console.log('Mongodb error', err);
    process.exit(1);
  });

function refreshToken() {
  login(username, password, "agent")
  .then(() => {
    console.log('refreshed token')
  })
  .catch(err => {
    console.log('error refreshing token', err);
    process.exit(1);
  })
}

// Refresh token every 10 mins
const refreshTokenJob = new CronJob({
  cronTime: "*/10 * * * *",
  onTick: () => {
    refreshToken();
  },
});

refreshTokenJob.start();


login(username, password, "agent")
  .then(() => {
    return createEventsChannel().then(() => {
      require("./events/listeners");
      app.post("/messages", (req, res) => {
        res.send("OK");

        if (!req.body.messages) return;

        const messages = req.body.messages;
        const messageType = messages[0].type;
        const messageSender = messages[0].from;
        const msg = {
          type: messageType,
          sender: messageSender,
        };
        if (messageType === "text") {
          msg.content = messages[0].text.body;
        } else if (messageType === "voice") {
          msg.content = messages[0].voice.id;
        } else {
          return sendToUser({
            textMessages: [
              "The available messages are text and voice messages only",
            ],
            to: msg.sender,
          });
        }

        handleMsg(msg)
          .then((response) => {
            sendToUser(response);
          })
          .catch(console.error);
      });
    });
  })
  .catch(console.error);

app.listen(port, () => console.log(`app listening on port ${port}`));
